<?php
define('HEADER_TITLE_1', "NUSAGATES INSTITUTE INDONESIA");
define('HEADER_TITLE_2', "DEPARTEMEN EDUMIN SALATIGA");
define('HEADER_TITLE_3', "Klampeyan RT 01/03 Noborejo Argomulyo Salatiga");
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('HOMEDIR', dirname(__FILE__));
define('K_PATH_IMAGES', HOMEDIR."/gambar");
date_default_timezone_set('Asia/Jakarta');
setlocale(LC_ALL, 'IND');
spl_autoload_register(function($class) {
	if (is_file($class = HOMEDIR . '/incl/' . str_replace('_', '/', $class) . '.php')){
		require($class);
    }
});
require HOMEDIR. '/vendor/autoload.php';

new Page(array(
	'rw_base' => '/edumin',
        'lang_dir' => HOMEDIR.'/incl/bahasa',
        'lang_def' => 'id_ID',
	'db_dsn' => 'mysql:host=localhost;dbname=NAMADATABASE',
	'db_user' => 'USER_DATABASE',
	'db_pswd' => 'PASS_DATABASE'
));
