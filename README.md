# Edumin

Edumin adalah apkilasi administrasi pendidikan untuk segala jenjang satuan pendidikan. Fitur-fitur edumin meliputi:
Manajemen sumber pemasukan, pengeluaran, membuat tagihan ke siswa tertentu, laporan keuangan, dll.

<h2>Konfigurasi Edumin</h2>
<ol>
 <li>Download repository ini ke komputer.</li>
 <li>Ekstrak file tersebut ke dalam direktori server, misalnya di dalam folder htdocs. Secara default file akan diekstrak di dalam folder <b>edumin</b>. </li>
 <li>Buat database baru dengan nama <b>edumin</b>. Import demo_edumin_org.sql ke database tersebut.</li>
 <li>Mengganti header laporan
  <ol>
   <li>Ganti <b>define('HEADER_TITLE_1', "NUSAGATES INSTITUTE INDONESIA");</b> menjadi <b>define('HEADER_TITLE_1', "HEADER 1 ANDA");</b></li>
   <li>Ganti <b>define('HEADER_TITLE_2', "NUSAGATES INSTITUTE INDONESIA");</b> menjadi <b>define('HEADER_TITLE_1', "HEADER 2 ANDA");</b></li>
   <li>Ganti <b>define('HEADER_TITLE_3', "NUSAGATES INSTITUTE INDONESIA");</b> menjadi <b>define('HEADER_TITLE_1', "HEADER 3 ANDA");</b></li>
  </ol>
 </li>
 <li>Buka file <b>index.php</b> di dalam folder edumin. ganti informasi database.
  <ul>
   <li>Ganti <b>NAMADATABASE</b> dengan nama edumin jika langkah ke 3 di atas Anda menggunakan nama edumin untuk membuat database. </li>
   <li>Ganti <b>USER_DATABASE</b> dengan pengguna database Anda. Misal: root</li>
   <li>Ganti <b>PASS_DATABASE</b> dengan password database Anda</li>
  </ul>
 </li>
 <li>Jika Anda mengikuti semua langkah di atas maka edumin bisa diakses menggunakan url: http://host/edumin. Contoh: http://localhot/edumin, http://edumin.org/edumin</li>
</ol>

<h2>Memindah folder instalasi edumin</h2>
<p>Sesuai petunjuk instalasi diatas, maka base url yang digunakan edumin adalah sub direktori. Jika Anda ingin memindahkan edumin ke root directory maka caranya adalah sebagai berikut:</p>
<ol>
 <li>Pindahkan semua file edumin ke root directory</li>
 <li>Buka <b>index.php</b> kemudian ganti <b>'/edumin'</b> di baris 19 menjadi <b>'/'</b></li>
 <li>Untuk pengguna Apache, buka .htaccess kemudian ubah isinya sebagai berikut:
  <ol>
   <li>Ganti <b>RewriteBase /edumin</b> di baris 13 menjadi <b>RewriteBase /</b></li>
   <li>Ganti <b>RewriteRule . /edumin/index\.php [L]</b> di baris 13 menjadi <b>RewriteRule . /index\.php [L]</b></li>
  </ol>
 </li>
 <li>Untuk pengguna Nginx, maka perlu menambahkan <b>if (-f $request_filename) { break; } try_files $uri $uri/ /index.php?$args;</b> di dalam konfigurasi blok. Umumnya konfigurasi ini terletak di dalam direktori <b>/etc/nginx/sites-available</b></li>
</ol>

<h2>Demonstrasi Edumin</h2>
<p>Demo edumin bisa diakses melalui url: http://demo.edumin.org. Untuk masuk ke sistem edumin sebagai admin bisa menggunakan kredensial berikut > email: <i>nusagates@gmail.com</i> password: <i>123456</i> </p>

<h2>Bantuan</h2>
<p>Jika ada kendala dalam menggunakan edumin maka Anda bisa menghubungi developer melalui email/skype nusagates@gmail.com, WA: 0822 2500 5825, atau buat thread di https://forum.nusagates.com</p>