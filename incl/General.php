<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class ini digunakan untuk menghandel fungsi-fungsi umum yang dibutuhkan aplikasi
 *
 * @author nusagates <nusagates@gmail.com>
 * @category fungsi
 * @version 1.0
 */
class General {

 public static function sidebar_menu($ctx) {
  $url = $ctx->req_uri;
  if (
          $url == "tahunajaran" ||
          $url == "biaya/kategori" ||
          $url == "pengeluaran/kategori" ||
          $url == "biaya/kategori/sumber" ||
          $url == "biaya/sumber" ||
          $url == "siswa" ||
          $url == "siswa/kelas" ||
          $url == "biaya"
  ) {
   $active1 = " active";
  } else if (
          $url == "rekap" ||
          $url == "laporan/kas" ||
          $url == "laporan"
  ) {
   $active2 = " active";
  }
  ?>

  <ul class="sidebar-menu">

   <li class="header text-center">MENU</li>
   <li class="treeview<?php echo $active1; ?>" >
    <a href="#">
     <i class="fa fa-database text-aqua"></i> <span>Data Master</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
     <li<?php echo $ctx->req_uri == "tahunajaran" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/tahunajaran" ?>"><i class="fa fa-circle-o text-aqua"></i> Tahun Ajaran</a></li>
     <li<?php echo $ctx->req_uri == "biaya/kategori" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/biaya/kategori" ?>"><i class="fa fa-circle-o text-aqua"></i> Kategori Pemasukan</a></li>
     <li<?php echo $ctx->req_uri == "pengeluaran/kategori" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/pengeluaran/kategori" ?>"><i class="fa fa-circle-o text-aqua"></i> Kategori Pengeluaran</a></li>
     <li<?php echo $ctx->req_uri == "biaya/kategori/sumber" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/biaya/kategori/sumber" ?>"><i class="fa fa-circle-o text-aqua"></i> Kategori Sumber Pemasukan</a></li>
     <li<?php echo $ctx->req_uri == "siswa/kelas" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/siswa/kelas" ?>"><i class="fa fa-circle-o text-aqua"></i> Master Kelas</a></li>
     <li<?php echo $ctx->req_uri == "siswa" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/siswa" ?>"><i class="fa fa-circle-o text-aqua"></i> Data Siswa</a></li>
     <li<?php echo $ctx->req_uri == "biaya/sumber" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/biaya/sumber" ?>"><i class="fa fa-circle-o text-aqua"></i> Sumber Pemasukan</a></li>
     <li<?php echo $ctx->req_uri == "biaya" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/biaya" ?>"><i class="fa fa-circle-o text-aqua"></i> Master Biaya</a></li>
    </ul>
   </li>
   <li class="treeview<?php echo $active2; ?>" >
    <a href="#">
     <i class="fa fa-bar-chart text-green"></i> <span>Laporan Keuangan</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
     <li<?php echo $ctx->req_uri == "rekap" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/rekap" ?>"><i class="fa fa-circle-o text-green"></i> Rekap Pembayaran</a></li>
     <li<?php echo $ctx->req_uri == "laporan/kas" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/laporan/kas" ?>"><i class="fa fa-circle-o text-green"></i> Arus Kas</a></li>
     <li<?php echo $ctx->req_uri == "laporan" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/laporan" ?>"><i class="fa fa-circle-o text-green"></i> Laporan Keuangan</a></li>
    </ul>
   </li>
   <li<?php echo $ctx->req_uri == "administrasi" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/administrasi" ?>"><i class="fa fa-circle-o text-aqua"></i> Input Pemasukan</a></li>
   <li<?php echo $ctx->req_uri == "pengeluaran" ? " class='active' " : '' ?>><a href="<?php echo $ctx->base_url . "/pengeluaran" ?>"><i class="fa fa-circle-o text-aqua"></i> Input Pengeluaran</a></li>
  </ul>
  <?php
 }

 /**
  * Fungsi ini digunakan untuk membuat variabel dari $_POST dan mengambil nilainya
  * @param string $label diisi dengan argumen/key $_POST atau value dari atribut name suatu input
  * @param variable $var nama variabelyang mau digunakan untuk mewakili $_POST
  * @return string value atau nilai dari $_POST
  */
 public static function s_post($label, &$var = null) {
  return empty($_POST[$label]) || '' === ($var = trim($_POST[$label]));
 }

 public static function menu($ctx, $key) {
  if ($ctx->req_uri == $key) {
   echo '<li class="active"><a href="' . $ctx->base_url . "/" . $key . '"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>';
  } else {
   echo '<li ><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>';
  }
 }

 public static function html_input($id, $label, $col = '6', $value = '', $required = '0', $type = 'text', $attr = '') {
  $req = $required == "1" ? "required='required'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id ?>"><?php echo $label ?></label>
    <input <?php echo $attr ?> <?php echo $req ?> value="<?php echo $value ?>" type="<?php echo $type ?>" class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"/>
   </div>
  </div>
  <?php
 }

 public static function html_select_db($db, $tabel, $id, $label, $col = '6', $queryClause = '', $selected = '', $attr = '') {
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id ?>"><?php echo $label ?></label>
    <select <?php echo $attr ?> class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>">
        <?php
        $q = $db->query("select * from $tabel $queryClause");
        if ($q->rowCount()) {
         while ($col = $q->fetch(PDO::FETCH_BOTH)) {
          $sel = $selected == $col[0] ? "selected='selected'" : "";
          echo "<option $sel value='$col[0]'>$col[1]</option>";
         }
        } else {
         echo "<option value=''>Tidak ada data...</option>";
        }
        ?>
    </select>
   </div>
  </div>
  <?php
 }

 public static function ambil_kelas($db, $label, $id = 'kelas', $col = '6', $selected = '') {
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id; ?>"><?php echo $label ?></label>
    <select class="form-control" name="kelas" id="<?php echo $id; ?>">
        <?php
        $q = $db->query("select * from kelas where status=1");
        while ($col = $q->fetchObject()) {
         $sel = $selected == $col->id ? "selected='selected'" : "";
         echo "<option $sel value='$col->id'>$col->nama</option>";
        }
        ?>
     <option value="0">Semua Kelas</option>
    </select>
   </div>
  </div>
  <?php
 }

 public static function html_input_hidden($id, $value = '') {
  ?>
  <input required="required" value="<?php echo $value ?> " style="display:none" name="<?php echo $id ?>" id="<?php echo $id ?>"/>
  <?php
 }

 public static function html_checkbox($id, $label, $col = '6', $checked = '0') {
  $check = $checked == '1' ? "checked='checked'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label><input <?php echo $check ?> name="<?php echo $id ?>" id="<?php echo $id ?>" type="checkbox"/> <?php echo $label ?></label>
   </div>
  </div>
  <?php
 }

 public static function html_textarea($id, $label, $col = '6', $value = '', $required = '0') {
  $req = $required == "1" ? "recuired='required'" : '';
  ?>
  <div class="col-md-<?php echo $col ?>">
   <div class="form-group">
    <label for="<?php echo $id ?>"><?php echo $label ?></label>
    <textarea <?php echo $req ?>  class="form-control" name="<?php echo $id ?>" id="<?php echo $id ?>"><?php echo $value ?> </textarea>
   </div>
  </div>
  <?php
 }

 public static function html_info($text = 'Silahkan isi data-data yang diperlukan.') {
  ?>
  <div class="col-md-12">
   <div class="text-info"><i class="fa fa-exclamation-circle"></i> <span class="info-text"><?php echo $text ?></span></div>
  </div>
  <?php
 }

 public static function html_modal_tambah() {
  ?>
  <div id="modal-tambah" class="modal fade" role="dialog" >
   <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Tambah</h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <form id="form-tambah" method="post">
           <?php
           General::html_input("nama", "Nama Kategori", 12, '', 1);
           General::html_textarea("keterangan", "Keterangan", 12, '');
           General::html_info();
           ?>

       </form>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function html_modal_edit($mdl_id = 'modal-edit', $mdl_type = "", $btn_id = 'btn-update', $btn_label = 'Update') {
  ?>
  <div id="<?php echo $mdl_id ?>" class="modal fade" role="dialog" >
   <div class="modal-dialog <?php echo $mdl_type ?>">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Edit</h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <div id="edit-form-container">
        <div class="col-md-12 text-center">
         <h2 class="fa fa-spin fa-spinner" style="font-size: 36px"></h2>
        </div>
       </div>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      <button id="<?php echo $btn_id ?>" type="button" class="btn btn-primary"><?php echo $btn_label ?></button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function html_modal_hapus() {
  ?>
  <div id="modal-hapus" class="modal fade" role="dialog" >
   <div class="modal-dialog">
    <div class="modal-content">
     <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      <h4 class="modal-title">Hapus</h4>
     </div>
     <div class="modal-body">
      <div class="row">
       <div id="hapus-form-container">
        <div class="col-md-12 text-center">
         <h2 class="fa fa-spin fa-spinner" style="font-size: 36px"></h2>
        </div>
       </div>
      </div>
     </div>
     <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
      <button id="btn-delete" type="button" class="btn btn-danger">Hapus</button>
     </div>
    </div>
   </div>
  </div>
  <?php
 }

 public static function error_PDO($db) {
  print_r($db->errorInfo());
 }

 public static function intohex($dec) {
  $hex = '';
  do {
   $last = bcmod($dec, 16);
   $hex = dechex($last) . $hex;
   $dec = bcdiv(bcsub($dec, $last), 16);
  } while ($dec > 0);
  return $hex;
 }

 public static function kas_masuk($ctx) {
  $query = $ctx->db->prepare("select sum(dibayar) kas_masuk from pemasukan where status=1");
  $query->execute();
  if ($query->rowCount()) {
   return $query->fetchObject()->kas_masuk;
  } else {
   return "0";
  }
 }

 public static function kas_keluar($ctx) {
  $query = $ctx->db->prepare("select sum(nominal) kas_keluar from pengeluaran where status=1");
  $query->execute();
  if ($query->rowCount()) {
   return $query->fetchObject()->kas_keluar;
  } else {
   return "0";
  }
 }
public static function item_dibayar($db, $id_siswa, $id_biaya) {
  $query = $db->prepare("SELECT sum(dibayar) dibayar FROM `pemasukan` WHERE siswa =? AND biaya=?");
  $query->execute(array($id_siswa, $id_biaya));
  if ($query->rowCount()) {
   return $query->fetchObject()->dibayar;
  } else {
   return "0";
  }
 }
public static function tahun_ajaran_aktif($db){
  $q = $db->prepare("SELECT id FROM `tahun_ajaran` WHERE status=?");
  $q->execute(array("1"));
  if ($q->rowCount()) {
   return $q->fetchObject()->id;
  } else {
   return "0";
  }
 }

}
