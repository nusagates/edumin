<?php
isset($this) || exit;

$title = "Laporan Keuangan";
$kas_masuk = General::kas_masuk($this);
$kas_keluar = General::kas_keluar($this);
$saldo = $kas_masuk - $kas_keluar;
ob_start();
?>

<div class="row">
 <div class="col-md-4">
  <div class="info-box">
   <span class="info-box-icon bg-teal-active"><i class="fa fa-plus-square"></i></span>
   <div class="info-box-content">
    <span class="info-box-text">Kas Masuk</span>
    <span class="info-box-number">Rp. <?php echo number_format($kas_masuk, 0, "", ",") ?> </span></span>
   </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
 </div>
 <div class="col-md-4">
  <div class="info-box">
   <span class="info-box-icon bg-yellow-active"><i class="fa fa-minus-square"></i></span>
   <div class="info-box-content">
    <span class="info-box-text">Kas Keluar</span>
    <span class="info-box-number">Rp. <?php echo number_format($kas_keluar, 0, "", ",") ?></span></span>
   </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
 </div>
 <div class="col-md-4">
  <div class="info-box">
   <span class="info-box-icon bg-green-active"><i class="fa fa-balance-scale"></i></span>
   <div class="info-box-content">
    <span class="info-box-text">Saldo</span>
    <span class="info-box-number">Rp. <?php echo number_format($saldo, 0, "", ",") ?></span></span>
   </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
 </div>

 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
    <div class="box-tools pull-right">
     <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
     <div></div></div>
   </div>
   <div class="box-body">
    <div class="row">
     <div class="col-md-4 col-md-offset-4 text-center">
         <?php General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 12) ?>
     </div>
     <div  class="col-md-12">
      <canvas id="grafik" style="height:100px"></canvas>
     </div>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->

 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Arus Kas</h3>
   </div>
   <div class="box-body">
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr><th style="text-align: center" colspan="3">LAPORAN KEUANGAN</th></tr>
        <tr>
         <td>BULAN</td>
         <td>KATEGORI</td>
         <td width="20%">DEBIT</td>
        </tr>
       </thead>
       <tbody>
           <?php
           $query = $this->db->query('SELECT * FROM(
SELECT "masuk" kas, b.tahun_ajaran tahun, monthname(m.tanggal) bulan, km.nama kategori, SUM(m.dibayar) total FROM `pemasukan` m 
LEFT JOIN biaya b 
ON m.biaya=b.id
LEFT JOIN kategori_pemasukan km 
ON b.kategori=km.id GROUP BY monthname(m.tanggal), b.kategori
UNION
SELECT "keluar" kas, k.tahun, monthname(k.tanggal) bulan,
kp.nama kategori, 
sum(k.nominal) total FROM `pengeluaran` k
LEFT JOIN kategori_pengeluaran kp
ON k.kategori=kp.id GROUP BY monthname(k.tanggal), k.kategori
) d WHERE tahun ="5"');
           if ($query->rowCount()) {
            $masuk = 0;
            $keluar = 0;
            $i = 0;
            while ($col = $query->fetchObject()) {
             $kas = $col->kas;
             if ($kas == "masuk") {
              $masuk += $col->total;
              echo "<tr>"
              . "<td>$col->bulan</td>"
              . "<td>$col->kategori</td>"
              . "<td style='text-align:right'>". number_format($col->total,0,",",",")."</td>"
              . "</tr>";
             } else if($kas == "keluar") {
              if ($i == 0) {
               echo "<tr><td colspan='2' style='text-align:center'><b>TOTAL DEBIT</b></td><td style='text-align:right'><b>Rp. ". number_format($masuk,0,",",",")."</b></td></tr>";
               echo "<tr>"
              . "<td>BULAN</td>"
              . "<td>KATEGORI</td>"
              . "<td>KREDIT</td>"
              . "</tr>";
              }
              $i++;
              echo "<tr>"
              . "<td>$col->bulan</td>"
              . "<td>$col->kategori</td>"
              . "<td style='text-align:right'>". number_format($col->total,0,",",",")."</td>"
              . "</tr>";
              $keluar +=$col->total;
             }
            }
            echo "<tr><td colspan='2' style='text-align:center'><b>TOTAL KREDIT</b></td><td style='text-align:right'><b>Rp. ". number_format($keluar,0,",",",")."</b></td></tr>";
           }
           ?>
       </tbody>
      </table>
     </div>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->
<script src="<?php echo $this->base_url . "/r/" ?>plugins/chartjs/Chart.min.js"></script>
<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/laporan';
     //fetch_data('#tabel', base_url + "/read");
     var ctx = document.getElementById("grafik").getContext('2d');
     var chart = new Chart(ctx, {
         type: 'bar',
         data: {
             labels: [""],
             datasets: [
                 {
                     label: 'Pemasukan',
                     data: [0],
                     backgroundColor: 'rgb(75, 192, 192)',
                     borderColor: 'rgb(75, 192, 192)',
                     fill: false
                 },
                 {
                     label: 'Pengeluaran',
                     data: [0],
                     backgroundColor: 'rgb(255, 99, 132)',
                     borderColor: 'rgb(255, 99, 132)',
                     fill: false
                 }
             ]
         },
         options: {
             title: {
                 display: true,
                 text: 'Grafik Kas Keuangan'
             },
             tooltips: {
                 mode: 'index',
                 intersect: false,
             },
             hover: {
                 mode: 'nearest',
                 intersect: true
             },
             scales: {
                 xAxes: [{
                         display: true,
                         scaleLabel: {
                             display: true,
                             labelString: 'Bulan'
                         }
                     }],
                 yAxes: [{
                         display: true,
                         scaleLabel: {
                             display: true,
                             labelString: 'Nominal'
                         },
                         ticks: {
                             userCallback: function (value, index, values) {
                                 // Convert the number to a string and splite the string every 3 charaters from the end
                                 value = value.toString();
                                 value = value.split(/(?=(?:...)*$)/);

                                 // Convert the array to a string and format the output
                                 value = value.join('.');
                                 return 'Rp.' + value;
                             }
                         }
                     }]
             },
             responsive: true
         }
     });
     var dataPemasukan = [];
     var dataPengeluaran = [];
     var dataBulan = [];
     $.ajax({
         url: base_url + "/data/chart",
         method: "post",
         dataType: "json",
         success: function (response) {
             for (var i = 0; i < response.length; i++) {
                 dataBulan.push(response[i].bulan);
                 dataPemasukan.push(response[i].masuk);
                 dataPengeluaran.push(response[i].keluar);
             }
             chart.data.datasets[0].data = dataPemasukan;
             chart.data.datasets[1].data = dataPengeluaran;
             chart.data.labels = dataBulan;
             chart.update();
             //alert(dataPemasukan)
         },
         error: function () {}
     });

     $("#tipe").change(function () {
         var tipe = $(this).val();
         chart.type.push(tipe);
         chart.update();
     });

 });
 function addCommas(nStr)
 {
     nStr += '';
     x = nStr.split('.');
     x1 = x[0];
     x2 = x.length > 1 ? '.' + x[1] : '';
     var rgx = /(\d+)(\d{3})/;
     while (rgx.test(x1)) {
         x1 = x1.replace(rgx, '$1' + ',' + '$2');
     }
     return x1 + x2;
 }

</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
