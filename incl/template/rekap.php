<?php
isset($this) || exit;

$title = "Rekap Pembayaran";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
   </div>
   <div class="box-body">
    <p>
     Rekap pembayaran menampilkan semua jenis pembayaran yang telah dilakukan oleh subjek tertagih beserta kekurangan atau kelebihannnya.
    </p>
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th width='5%'>NISN</th>
         <th>NAMA</th>
         <th>BIAYA</th>
         <th>NOMINAL</th>
         <th width='14%'>DIBAYAR</th>
         <th>KURANG/LEBIH</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
     <div id="modal-tambah" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-sm">
       <div class="modal-content">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Tambah</h4>
        </div>
        <div class="modal-body">
         <div class="row">
          <form id="form-tambah" method="post">
              <?php
              General::html_input("nama", "Nama Pembiayaan", 12, '', '1', "text", "placeholder='contoh: SPP'");
              General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 12, "where status=1");
              General::html_select_db($this->db, "bulan", "bulan", "Bulan Tagihan", 12);
              General::html_input("nominal", "Nominal", 12, '', '1', "number");
              General::html_checkbox("status", "Aktif", 12, 1);
              General::html_info();
              ?>

          </form>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
         <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
        </div>
       </div>
      </div>
     </div>
     <?php
     General::html_modal_edit("modal-edit", "modal-sm");
     General::html_modal_hapus();
     ?>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/rekap';
     var id_siswa = [];
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         update_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#btn-update-kelas").click(function () {
         var data = $("#form-naik-kelas").serialize();
         $.ajax({
             url: base_url + "/naik/update",
             method: 'post',
             data: data,
             success: function (s) {
                 $('.info-text').slideDown();
                 $('.info-text').text(s);
                 if (s === '1') {
                     $('.info-text').text('memroses');
                     $('#modal-naik-kelas').modal("hide");
                     $('#tabel').DataTable().ajax.reload();
                     $("#btn-naik-kelas").slideUp();
                     $("#btn-hapus-select").slideUp();
                 }
             }
         });
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });
     $("#tabel").on("click", ".pilih-siswa", function () {
         var values = $('#tabel input:checkbox:checked').map(function () {
             return this.value;
         }).get();
         if (values.length > 0) {
             $("#btn-naik-kelas").slideDown();
             $("#btn-hapus-select").slideDown();
         } else {
             $("#btn-naik-kelas").slideUp();
             $("#btn-hapus-select").slideUp();
         }
         id_siswa = values;
     });

     $("#btn-naik-kelas").click(function () {
         $("#modal-naik-kelas").modal("show");
         edit_form(base_url + "/naik/form", "#modal-naik-kelas", id_siswa, '#modal-naik-kelas #edit-form-container');
     });
     $("#btn-hapus-select").click(function () {
         $('#tabel input:checkbox:checked').removeAttr('checked');
         $("#btn-naik-kelas").slideUp();
         $("#btn-hapus-select").slideUp();
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
