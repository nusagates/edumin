<?php
isset($this) || exit;

$title = "Arus Kas Keuangan";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
    <div class="pull-right">
     <span class="input-group-btn">
      <button id="export-pdf" class="btn btn-xs btn-default bg-orange" type="button"><i class="fa fa-file-pdf-o"></i> PDF</button>
     </span>
     <span class="input-group-btn">
      <button id="export-excel" class="btn btn-xs btn-default bg-green" type="button"><i class="fa fa-file-excel-o"></i> EXCEL</button>
     </span>
    </div>
   </div>
   <div class="box-body">
    <div class="row">
     <div class="col-md-6 col-md-offset-3">
      <div class="input-group">
       <form method="post" >
        <div class="col-md-6">
         <input name="mulai" id="mulai" placeholder="Tanggal Mulai" type="text" class="form-control">
       </div>
       <div class="col-md-6">
        <input name="akhir" id="akhir" placeholder="Tanggal Berakhir" type="text" class="form-control">
       </div>
       </form>
       <span class="input-group-btn">
        <button id="filter" class="btn btn-default" type="button">Filter</button>
       </span>

      </div>
     </div>
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>TANGGAL</th>
         <th>URAIAN</th>
         <th>KAS MASUK</th>
         <th>KAS KELUAR</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/laporan/kas';
     var id_siswa = [];
     var tabel;

     $('#mulai').datetimepicker({
         timepicker: false,
         format: 'Y-m-d'
     });
     $('#akhir').datetimepicker({
         timepicker: false,
         format: 'Y-m-d'
     });
     tabel = $("#tabel").DataTable({
         order: [[1, 'asc']],
         "ajax": {
             "url": base_url + "/read",
             "type": "post"
         }
     });
     $("#filter").click(function () {
         var t_mulai = $("#mulai").val();
         var t_akhir = $("#akhir").val();
         $.ajax({
             url: base_url + "/read",
             type: "post",
             dataType: 'json',
             data: {mulai: t_mulai, akhir: t_akhir},
             success: function (response) {
                 //alert(response.data)
                 tabel.clear().draw();
                 for (var i = 0; i < response.data.length; i++) {
                     tabel.row.add([
                         response.data[i][0],
                         response.data[i][1],
                         response.data[i][2],
                         response.data[i][3]
                     ]).draw(false);
                 }
                 //
             },
             error: function () {}
         });

     });

     $("#export-pdf").click(function () {
       $("form").attr("action", base_url+"/cetak/pdf")
       $("form").submit();
     });
     $("#export-excel").click(function () {
       $("form").attr("action", base_url+"/cetak/excel")
       $("form").submit();
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
