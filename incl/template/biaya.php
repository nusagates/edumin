<?php
isset($this) || exit;

$title = "Master Biaya";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
   </div>
   <div class="box-body">
    <p>
     Master biaya digunakan untuk mengelola item-item pembiayaan terkait kegiatan sekolah yang dibebankan pada siswa.
    </p>
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>NAMA</th>
         <th>NOMINAL</th>
         <th>TAHUN AJARAN</th>
         <th width="5%">BULAN</th>
         <th width="5%">STATUS</th>
         <th width="5%">Aksi</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
     <div class="col-md-12">
      <div class="form-group">
       <button style="margin-bottom: 5px;" data-toggle="modal" data-target="#modal-tambah" class="btn bg-aqua-active">Tambah Item Pembayaran</button>
      </div>
     </div>
     <div id="modal-tambah" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-md">
       <div class="modal-content">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Tambah</h4>
        </div>
        <div class="modal-body">
         <div class="row">
          <form id="form-tambah" method="post">
              <?php
              General::html_select_db($this->db, "kategori_pemasukan", "kategori", "Kategori Pemasukan", 6, "where status=1");
              General::html_input("nama", "Nama Pembiayaan", 6, '', '1', "text", "placeholder='contoh: SPP'");
              General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1");
              General::html_select_db($this->db, "bulan", "bulan", "Bulan Tagihan", 6);
              General::html_input("nominal", "Nominal", 6, '', '1', "number");
              General::ambil_kelas($this->db, "Filter Tertagih", "kelas",6);
              echo "<div class='col-md-12'>"
              . "<div class='form-group'>"
              . "<fieldset>"
              . "<legend>Hasil Filter</legend>"
              . "<div class='form-inline' id='hasil-filter'></div>"
              . "<button class='btn btn-info' id='btn-tagih'>Tambahkan ke Daftar Tertagih</button>"
              . "</fieldset>"
              . "</div>"
              . "</div>";
              echo "<div class='col-md-12'>"
              . "<div class='form-group'>"
              . "<fieldset>"
              . "<legend>Daftar Tertagih</legend>"
              . "<div class='form-inline' id='siswa-tertagih'></div>"
              . "</fieldset>"
              . "</div>"
              . "</div>";
              General::html_info();
              ?>

          </form>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
         <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
        </div>
       </div>
      </div>
     </div>
     <?php
     General::html_modal_edit("modal-edit", "modal-md");
     General::html_modal_hapus();
     ?>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/biaya';
     var id_siswa = [];
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         update_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#btn-update-kelas").click(function () {
         var data = $("#form-naik-kelas").serialize();
         $.ajax({
             url: base_url + "/naik/update",
             method: 'post',
             data: data,
             success: function (s) {
                 $('.info-text').slideDown();
                 $('.info-text').text(s);
                 if (s === '1') {
                     $('.info-text').text('memroses');
                     $('#modal-naik-kelas').modal("hide");
                     $('#tabel').DataTable().ajax.reload();
                     $("#btn-naik-kelas").slideUp();
                     $("#btn-hapus-select").slideUp();
                 }
             }
         });
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });
     $("#tabel").on("click", ".pilih-siswa", function () {
         var values = $('#tabel input:checkbox:checked').map(function () {
             return this.value;
         }).get();
         if (values.length > 0) {
             $("#btn-naik-kelas").slideDown();
             $("#btn-hapus-select").slideDown();
         } else {
             $("#btn-naik-kelas").slideUp();
             $("#btn-hapus-select").slideUp();
         }
         id_siswa = values;
     });

     $("#btn-naik-kelas").click(function () {
         $("#modal-naik-kelas").modal("show");
         edit_form(base_url + "/naik/form", "#modal-naik-kelas", id_siswa, '#modal-naik-kelas #edit-form-container');
     });
     $("#btn-hapus-select").click(function () {
         $('#tabel input:checkbox:checked').removeAttr('checked');
         $("#btn-naik-kelas").slideUp();
         $("#btn-hapus-select").slideUp();
     });

     $("#kelas").change(function () {
         var id = $(this).val();
         getSiswa(id);
     });
     getSiswa("1");
     function getSiswa(id) {
         $.ajax({
             url: base_url + "/siswa",
             dataType: 'text',
             method: "post",
             data: {filter: id},
             success: function (response) {
                 $("#hasil-filter").html(response);
                 $("#btn-tagih").click(function (e) {
                     e.preventDefault();
                     var data = $("#hasil-filter input:checkbox:checked");
                     data.parent().appendTo("#siswa-tertagih");
                     $("#siswa-tertagih input:checkbox").attr("name", "id-siswa[]");
                     $("#siswa-tertagih input:checkbox").change(function () {
                         if (!$(this).is(":checked")) {
                             $(this).removeAttr("name");
                             $(this).parent().appendTo("#hasil-filter");
                         }
                     });
                 });
             },
             error: function () {}
         });
     }


 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
