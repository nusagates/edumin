<?php
isset($this) || exit;

$title = "Administrasi Keuangan";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Input Data</h3>
   </div>
   <div class="box-body">
    <p>
     Modul ini digunakan untuk menambahkan data pembayaran siswa. Gunakan kolom nama siswa untuk mengisi NISN dan kelas.
    </p>
    <div class="row">
     <form id="form-tambah">
      <div class="col-md-6">
          <?php
          General::html_input_hidden('id-siswa', "0");
          General::html_input("nama", "Nama Siswa", 12, '', '1', "text");
          General::html_input("nisn", "NISN", 6, '', '1', "text", "readonly");
          General::html_input("kelas", "Kelas", 6, '', '1', "text", "readonly");
          ?>
      </div>
      <div class="col-md-6">
          <?php
          General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1");
          echo "<div class='col-md-6'>"
          . "<div class='form-group'>"
          . "<label for='biaya'>Item Biaya</label>"
          . "<select name='biaya' id='biaya' class='form-control'>"
          . "<option>Pilih Siswa Dulu...</option>"
          . "</select>"
          . "</div>"
          . "</div>";
          General::html_input("dibayar", "Jumlah Pembayaran", 6, '', '1', "number");
          General::html_input("tanggal", "Tanggal Pembayaran", 6, '', '1', "datetime");
          ?>
       <hr/>
      </div>
     </form>

     <div class="col-md-6 col-md-offset-3">
      <div class="text-center">
       <button style="margin-bottom: 5px;" id="btn-tambah"  class="btn bg-aqua-active">Proses Pembayaran</button>
      </div>
     </div>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->

 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
   </div>
   <div class="box-body">
    <p>
     Tabel di bawah ini berisi daftar pembayaran yang sudah dimasukkan.
    </p>
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>NAMA</th>
         <th>ITEM PEMBAYARAN</th>
         <th>JUMLAH</th>
         <th>TANGGAL</th>
         <th>ADMIN</th>
         <th width="5%">Aksi</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
     <div id="modal-tambah" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-sm">
       <div class="modal-content">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Tambah</h4>
        </div>
        <div class="modal-body">
         <div class="row">
          <form method="post">
              <?php
              General::html_input("nama", "Nama Pembiayaan", 12, '', '1', "text", "placeholder='contoh: SPP'");
              General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 12, "where status=1");
              General::html_select_db($this->db, "bulan", "bulan", "Bulan", 12);
              General::html_checkbox("status", "Aktif", 12, 1);
              General::html_info();
              ?>

          </form>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
         <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
        </div>
       </div>
      </div>
     </div>
     <?php
     General::html_modal_edit("modal-edit", "modal-sm");
     General::html_modal_hapus();
     ?>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/administrasi';
     var id_siswa = [];
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         update_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });
     $('#nama').autocomplete({
         source: function (request, response) {
             $.ajax({
                 url: base_url + "/autocomplete",
                 method: 'post',
                 dataType: "json",
                 data: {term: request.term},
                 success: function (s) {
                     response(s);
                 }
             });
         },
         minLength: 2,
         delay: 100,
         select: function (event, ui) {
             //alert(ui.item.nisn);
             $("#nisn").val(ui.item.nisn);
             $("#kelas").val(ui.item.kelas);
             $("#id-siswa").val(ui.item.id);
             $("#nama").val(ui.item.label);
             var th = $("#tahun").val();
             getItemBiaya(ui.item.id, th);
             return false;
         }
     });

     $("#tahun").change(function () {
         var th = $(this).val();
         var idSiswa = $("#id-siswa").val();
         
         if (idSiswa ==="0") {
           swal("Peringatan!","Silahkan pilih siswa dulu.", "warning");
         } else {
           getItemBiaya(idSiswa, th);
         }
         //alert(id)
     });

     function getItemBiaya(idSiswa, th) {
         $.ajax({
             url: base_url + "/biaya",
             method: 'post',
             dataType: "text",
             data: {id: idSiswa, tahun: th},
             success: function (s) {
                 $("#biaya").html(s);
             }
         });
     }

     $('#tanggal').datetimepicker({
         timepicker: true,
         format: 'Y-m-d H:i:s'
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
