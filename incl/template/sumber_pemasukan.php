<?php
isset($this) || exit;

$title = "Sumber Pemasukan";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
   </div>
   <div class="box-body">
    <p>
     Sumber pemasukan bisa diisi dengan macam-macam sumber pemasukan. Misal: daftar siswa yang membayar SPP atau lainnya, nama-nama donatur, macam-macam bantuan biaya operasional
    </p>
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>KODE</th>
         <th>NAMA</th>
         <th>KONTAK</th>
         <th>KATEGORI</th>
         <th width="5%">Aksi</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
     <div class="col-md-12">
      <div class="form-group">
       <button style="margin-bottom: 5px;" data-toggle="modal" data-target="#modal-tambah" class="btn bg-aqua-active">Tambah</button>
      </div>
     </div>
     <div id="modal-tambah" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-md">
       <div class="modal-content">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Tambah</h4>
        </div>
        <div class="modal-body">
         <div class="row">
          <form id="form-tambah" method="post">
              <?php
              General::html_select_db($this->db, "kategori_sumber_pemasukan", "kategori", "Kategori", 6, "where status=1");
              General::html_input("kode", "KODE/NISN", 6, '', '1', "number");
              General::html_input("nama", "Nama", 6, '', '1', "text");
              General::html_select_db($this->db, "kelas", "kelas", "Kelas", 6, "where status=1");
              echo "<div id='additional-container'>";
              General::html_select_db($this->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1");
              General::html_input("nama-wali", "Nama Wali", 6, "", '1', "text");
              echo "</div>";
              General::html_input("hp-wali", "No. HP", 6, "", '1', "tel");
              General::html_textarea("alamat", "Alamat", "12");
              General::html_info();
              ?>

          </form>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
         <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
        </div>
       </div>
      </div>
     </div>
     <?php
     General::html_modal_edit("modal-edit", "modal-md");
     General::html_modal_hapus();
     ?>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/biaya/sumber';
     var id_siswa = [];
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         update_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });
     $("#modal-tambah #kategori").change(function () {
         var kategori_id = $(this).val();
         if (kategori_id !== "1") {
             $("#additional-container").slideUp();
         } else {
           $("#additional-container").slideDown();
         }
     });
     

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
