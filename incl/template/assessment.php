<?php
isset($this) || exit;

$title = "Buat Penilaian";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Input Nilai</h3>
   </div>
   <div class="box-body">
    <p>
     Silahkan isi masing-masing nilai kriteria di bawah ini dengan <b>skala penilaian yang sama</b> untuk semua karyawan (misal: antara 1 sampai 10). 
     Hasil penghitungan penilaian ditampilkan pada tabel di bawah. 3 Hasil terbaik secara default ditampilkan pada tabel. Gunakan filter untuk menampilkan hasil lebih banyak
    </p>
    <div class="row">
     <div class="col-md-12">
      <form id="form-tambah" method="post">
          <?php
          $criteria = $this->db->prepare("select * from criteria");
          $criteria->execute();
          if ($criteria->rowCount()) {
           echo "<div class='col-md-6 col-md-offset-3 text-center'>";
           echo General::html_input("name", "Nama Karyawan", 12, '', 1);
           echo "</div>";
           echo "<div class='col-md-12'><fieldset>";
           echo "<legend>Kriteria Penilaian</legend>";
           while ($col = $criteria->fetchObject()) {
            echo General::html_input(strtolower($col->name) . "-" . $col->id, $col->name, 2, '', 1, "number");
           }
           echo "</fieldset></div>";
           echo General::html_info();
           ?>
        <div class="col-md-12"><hr></div>
        <div class="col-md-12">
         <div class="form-group">
          <button id="button-tambah" class="btn bg-aqua-active">Hitung Nilai</button>
         </div>
        </div>
        <?php
       } else {
        ?>

        <div class="alert alert-warning">
         <h4><i class="icon fa fa-warning"></i> Perhatian!</h4>
         Tidak ada kriteria penilaian untuk diisi. Silahkan buat terlebih dahulu pada halaman Kriteria Penilaian. Gunakan navigasi di samping untuk membuka halaman tersebut.
        </div>
        <?php
       }
       ?>
      </form>
     </div>



    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->

 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Hasil Penghitungan</h3>
   </div>
   <div class="box-body">
    <div class="col-md-12">
     <table id="tabel" class="table table-bordered">
      <thead>
       <tr>
        <th data-orderable="false" width="5%">No.</th>
        <th data-orderable="false">Nama Karyawan</th>
        <th data-orderable="false">Total Nilai</th>
        <th data-orderable="false" width="5%">Aksi</th>
       </tr>
      </thead>
      <tbody>

      </tbody>
     </table>
    </div>
   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->
<?php General::html_modal_hapus(); ?>
<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/assessment';
     var tabel = $('#tabel').DataTable( {
        "processing": true,
        "lengthMenu": [[3, 10, 25, 50, -1], [3,10, 25, 50, "All"]],
        "paging": true,
        "serverSide": true,
        "ajax":{ 
           "url": base_url + "/read",
           "type": "post"
        }
    } );

     $("#button-tambah").click(function (e) {
         e.preventDefault();
         var data = $("#form-tambah").serialize();

         //add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
         $('.info-text').hide();
         $.ajax({
             url: base_url + "/insert",
             method: 'post',
             data: data,
             success: function (s) {
                 $('.info-text').slideDown();
                 $('.info-text').text(s);
                 if (s === '1') {
                     $("#form-tambah").trigger('reset');
                     $('.info-text').text("Penilaian berhasil dibuat. Silahkan lihat  hasilnya pada tabel di bawah.");
                     $('#tabel').DataTable().ajax.reload();
                 }
             }
         });
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         add_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });

 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
