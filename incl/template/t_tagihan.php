<?php
isset($this) || exit;
$db = $this->db;

$qs = $db->prepare("SELECT sp.kode, sp.nama, kl.nama kelas FROM `sumber_pemasukan` sp
LEFT JOIN kelas kl
ON sp.kelas_sekarang=kl.id
WHERE sp.id=?");
$qs->execute(array($this->req_sub));
$siswa = $qs->fetchObject();
$title = "Tagihan Siswa";
ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title"><?php echo $siswa->nama ?></h3>
    <div id="cetak" class="pull-right">
     <span class="input-group-btn">
       <button id="export-pdf" class="btn btn-xs btn-default bg-orange" type="button"><i class="fa fa-file-pdf-o"></i> PDF</button>
      </span>
    </div>
   </div>
   <div class="box-body">
    <p>
    <dl>
     <dt>NISN</dt>
     <dd><?php echo $siswa->kode ?></dd>
     <dt>KELAS</dt>
     <dd><?php echo $siswa->kelas ?></dd>
    </dl>
    </p>
    <div class="row">
     <div class="col-md-6 col-md-offset-3">
         <?php General::html_select_db($db, "tahun_ajaran", "tahun", "Tahun Ajaran", "12", "", General::tahun_ajaran_aktif($this->db)) ?> 
     </div>
     <div class="col-md-12">
      
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>TAGIHAN</th>
         <th>NOMINAL</th>
         <th>DIBAYAR</th>
         <th>KETERANGAN</th>
        </tr>
       </thead>
       <tbody>

       </tbody>
      </table>
     </div>

     <form id="cetak" method="post">
      <input type="hidden" name="siswa" id="idSiswa"/>
      <input type="hidden" name="tahun" id="idTahun"/>
     </form>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/tagihan';
     var idSiswa = "<?php echo $this->req_sub ?>";
     var idTahun = $("#tahun").val();
     getData(idSiswa, idTahun);
     function getData(idSiswa, idTahun) {
         $.ajax({
             url: base_url + "/tahun",
             method: 'post',
             data: {id: idSiswa, tahun: idTahun},
             success: function (data) {
                if(data=="0"){
                 $("tbody").html("<tr><td style='text-align:center' colspan='4'>Tidak ada data tagihan</td></tr>");
                 $("#cetak").slideUp();
                }else{
                 $("tbody").html(data);
                 $("#cetak").slideDown();
                }
             }
         });
     }
     $("#tahun").change(function () {
         getData(idSiswa, $(this).val());
     });
     $("#export-pdf").click(function(){
      $("form").attr("action", base_url+"/pdf");
      $("#idSiswa").val(idSiswa);
      $("#idTahun").val($("#tahun").val());
       $("form").submit();
     });
 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
