<?php
isset($this) || exit;

$title = "Kategori Sumber Pemasukan";

ob_start();
?>

<div class="row">
 <div class="col-md-12">
  <div class="box box-primary">
   <div class="box-header">
    <i class="fa fa-edit"></i>
    <h3 class="box-title">Data Masuk</h3>
   </div>
   <div class="box-body">
    <p>
     Ketegori sumber pemasukan digunakan untuk membuat kategorisasi sumber pemasukan Misal: <b>Siswa, Donasi, BOS</b>
    </p>
    <div class="row">
     <div class="col-md-12">
      <table id="tabel" class="table table-bordered">
       <thead>
        <tr>
         <th>NAMA</th>
         <th width="5%">Aksi</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
     <div class="col-md-12">
      <div class="form-group">
       <button style="margin-bottom: 5px;" data-toggle="modal" data-target="#modal-tambah" class="btn bg-aqua-active">Tambah</button>
      </div>
     </div>
     <div id="modal-tambah" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-sm">
       <div class="modal-content">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
         <h4 class="modal-title">Tambah</h4>
        </div>
        <div class="modal-body">
         <div class="row">
          <form id="form-tambah" method="post">
              <?php
              General::html_input("nama", "Nama Kategori", 12, '', '1', "text", "placeholder='contoh: SPP'");
              General::html_info();
              ?>

          </form>
         </div>
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
         <button id="btn-tambah" type="button" class="btn btn-primary">Tambah</button>
        </div>
       </div>
      </div>
     </div>
     <?php
     General::html_modal_edit("modal-edit", "modal-sm");
     General::html_modal_hapus();
     ?>
    </div>

   </div><!-- /.box-body -->
  </div><!-- /.box -->
 </div><!-- /.col -->
</div><!-- /. row -->

<script>
 $(function () {
     'use strict';
     var base_url = '<?php echo $this->base_url ?>/biaya/kategori/sumber';
     var id_siswa = [];
     fetch_data('#tabel', base_url + "/read");

     $("#btn-tambah").click(function () {
         var data = $("#form-tambah").serialize();
         add_data(base_url + "/insert", data, '.info-text', '#modal-tambah', '#tabel');
     });

     $("#tabel").on("click", ".btn-edit", function (e) {
         var id = $(this).attr("data-edit");
         e.preventDefault();
         edit_form(base_url + "/edit/form", "#modal-edit", id, '#edit-form-container');
     });

     $("#btn-update").click(function () {
         var data = $("#form-edit").serialize();
         update_data(base_url + "/update", data, '.info-text', '#modal-edit', '#tabel');
     });
     $("#tabel").on("click", ".btn-hapus", function (e) {
         var id = $(this).attr("data-hapus");
         e.preventDefault();
         delete_form(base_url + "/delete/form", "#modal-hapus", id, '#hapus-form-container');
     });
     $("#btn-delete").click(function () {
         var data = $("#form-hapus").serialize();
         remove(base_url + "/delete", data, '#modal-hapus', "#tabel");
     });


 });
</script>

<?php
$content = ob_get_clean();
require dirname(__FILE__) . '/dashboard.php';
?>
