<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author nusag
 */
class User {
 public static function isLogin() {
  return isset($_SESSION['id-pengguna']) ? true : false;
 }
 
 public static function getLogin($ctx){
  if(!User::isLogin()){
   header("location:".$ctx->base_url."/gate");exit;
  }
 }
 
 public static function isAuthorized($group){
  return User::user_group()==$group?true:false;
 }
 
 public static function unAuthorized(){
  header("Status: 401", true, 401);
 }
 
 public static function user_image_url($base){
  if(is_file(HOMEDIR."/gambar/profil/".User::userIdHex().".jpg")){
   return $base."/gambar/profil/".User::userIdHex().".jpg";
  }else{
   return $base."/gambar/profil/default.jpg";
  }
 }

 public static function display_name() {
  return isset($_SESSION['nama-pengguna']) ? $_SESSION['nama-pengguna'] : 'No Display';
 }

 public static function user_group() {
  return isset($_SESSION['grup-pengguna']) ? $_SESSION['grup-pengguna'] : 'No Group';
 }
 
 public static function getUserId(){
  return isset($_SESSION['id-pengguna']) ? $_SESSION['id-pengguna'] : '0';
 }
 
 public static function userIdHex(){
  return General::intohex(User::getUserId()+1000000);
 }

}
