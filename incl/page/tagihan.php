<?php

class page_tagihan {

 public $ctx;

 function __construct($ctx) {
  $query = $ctx->req_sub;
  $this->ctx = $ctx;
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  } else {
   if ($query != "tahun" && $query != "pdf") {
    if (preg_match("!^[A-Z a-z 0-9]+$!", $query)) {
     $q = $ctx->db->prepare("SELECT * FROM `biaya` WHERE find_in_set(?,siswa)");
     $q->execute(array($query));
     if ($q->rowCount()) {
      $ctx->_load_template($this, 't_tagihan');
     } else {
      echo "tidak ada";
     }
    }
   }
  }
 }

 function tahun() {
  if (General::s_post("id", $id))
   exit(Text('required', "ID"));
  if (General::s_post("tahun", $tahun))
   exit(Text('required_select', "Tahun"));
  $db = $this->ctx->db;
  $qb = $db->prepare("SELECT id, nama, nominal FROM `biaya` WHERE find_in_set(?, siswa) AND tahun_ajaran=?");
  $qb->execute(array($id, $tahun));
  if ($qb->rowCount()) {
   while ($biaya = $qb->fetchObject()) {
    $nominal = $biaya->nominal;
    $item_dibayar = General::item_dibayar($db, $id, $biaya->id);
    $dibayar = $item_dibayar == "" ? "0" : number_format($item_dibayar, 0, "", ",");
    $keterangan = "";
    $status = number_format($nominal - $item_dibayar, 0, "", ",");
    if ($item_dibayar == "") {
     $keterangan = "<span class='label label-danger'>Belum Dibayar</span>";
    } else if ($item_dibayar < $nominal && $item_dibayar != "0") {
     $keterangan = "<span class='label label-warning'>Kurang $status </label>";
    } else if ($item_dibayar == $nominal) {
     $keterangan = "<span class='label label-success'>Lunas</span>";
    } else {
     $keterangan = "Lebih $status";
    }
    echo "<tr>"
    . "<td>$biaya->nama</td>"
    . "<td>" . number_format($nominal, 0, "", ",") . "</td>"
    . "<td>$dibayar</td>"
    . "<td>$keterangan</td>"
    . "</tr>";
   }
  } else {
   echo "0";
  }
 }

 function pdf() {
  if (General::s_post("siswa", $id))
   exit(Text('required', "ID"));
  if (General::s_post("tahun", $tahun))
   exit(Text('required_select', "Tahun"));
  $title = "Tagihan Siswa";
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $pdf->SetHeaderData('/logo.jpg', 20, HEADER_TITLE_1, HEADER_TITLE_2 . "\n" . HEADER_TITLE_3, array(0, 0, 0), array(0, 64, 128));
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 14));
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Edumin Indonesia');
  $pdf->SetTitle('Tagihan Siswa');
  $pdf->SetSubject('Administrasi Keuangan');
  $pdf->SetKeywords('Laporan Keuangan, Administrasi Sekolah');

  $pdf->AddPage();
  $db = $this->ctx->db;
  $qb = $db->prepare("SELECT id, nama, nominal FROM `biaya` WHERE find_in_set(?, siswa) AND tahun_ajaran=?");
  $qb->execute(array($id, $tahun));
  $qs = $db->prepare("SELECT sp.kode, sp.nama, kl.nama kelas FROM `sumber_pemasukan` sp
LEFT JOIN kelas kl
ON sp.kelas_sekarang=kl.id
WHERE sp.id=?");
  $qs->execute(array($id));
  $siswa = $qs->fetchObject();
  $html = '<html><head></head><body>
          <style>
          table, td,  th{border: 1px solid #eee; padding: 2px}
          .number{text-align: right}
          .foot, .head{background-color: #eee}
          .document-title{text-align:center;margin-top:10px;}
          .document-title div{font-size: 14pt; text-decoration: underline}
          </style>
          <div class="document-title">
          <div>REKAP TAGIHAN SISWA</div></div>
          <table style="border: none !important;"> 
          <tr><td style="border: none !important;"  width="50px">Nama</td><td style="border: none !important;">: '.$siswa->nama.'</td></tr>
          <tr><td style="border: none !important;" width="50px">NISN</td><td style="border: none !important;">: '.$siswa->kode.'</td></tr>
          <tr><td style="border: none !important;" width="50px">Kelas</td><td style="border: none !important;">: '.$siswa->kelas.'</td></tr>
          </table> <div style="clear:both"></div>
          <table  id="tabel" width="640" cellspacing="0" >
          <thead>
          <tr class="head">
          <th>TAGIHAN</th>
         <th>NOMINAL</th>
         <th>DIBAYAR</th>
         <th>KETERANGAN</th>
          </tr> 
          </thead>
          <tbody>';
  if ($qb->rowCount()) {
   while ($biaya = $qb->fetchObject()) {
    $nominal = $biaya->nominal;
    $item_dibayar = General::item_dibayar($db, $id, $biaya->id);
    $dibayar = $item_dibayar == "" ? "0" : number_format($item_dibayar, 0, "", ",");
    $keterangan = "";
    $status = number_format($nominal - $item_dibayar, 0, "", ",");
    if ($item_dibayar == "") {
     $keterangan = "<span class='label label-danger'>Belum Dibayar</span>";
    } else if ($item_dibayar < $nominal && $item_dibayar != "0") {
     $keterangan = "<span class='label label-warning'>Kurang $status </label>";
    } else if ($item_dibayar == $nominal) {
     $keterangan = "<span class='label label-success'>Lunas</span>";
    } else {
     $keterangan = "Lebih $status";
    }
    $html .= "<tr>"
            . "<td>$biaya->nama</td>"
            . "<td>" . number_format($nominal, 0, "", ",") . "</td>"
            . "<td>$dibayar</td>"
            . "<td>$keterangan</td>"
            . "</tr>";
   }
  }
  $html .='<br/><br/><div style="width:300px;margin-left:400px"> '
          . '<div>'
          . 'Salatiga, '. date("d F Y").' <br>'
          . 'Bendahara <br><br><br><br>'
          . '. . . . . . . . . . . . '
          . '</div>'
          . '</div>';
  $html .= "<tbody></table></body></html>";
  //echo $html;
  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  $pdf->Output($title . '.pdf', 'I');
 }

}
