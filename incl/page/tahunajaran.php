<?php

class page_tahunajaran {

 public $ctx;
 private $table = 'tahun_ajaran';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'tahunajaran');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("select * from $this->table where status !=10 order by keterangan asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("select * from $this->table where status !=10 and (keterangan like) order by keterangan asc limit $start, $length");
   $query->execute(array("%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $status = $col->status=="1"?"Aktif":"Tidak Aktif";
   $data[]=array($col->keterangan, $status, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('keterangan', $keterangan))exit(text('required', 'Keterangan'));
  General::s_post('status', $status);
  if(empty($status)){
   $status="0";
  }else{
   $status ="1";
  }
  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(keterangan, status) values(?,?)");
  if ($query->execute(array($keterangan, $status))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("keterangan", "Keterangan", 12, $col->keterangan, '1', "text");
   General::html_checkbox("status", "Aktif", 12, $col->status);
   General::html_info();
   echo '</form>';
  }
 }

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('keterangan', $keterangan))exit(text('required', 'Keterangan'));
  General::s_post('status', $status);
  if(empty($status)){
   $status="0";
  }else{
   $status ="1";
  }
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set keterangan=?, status=? where $this->id=?");
  if ($query->execute(array($keterangan, $status, $id))) {
   echo '1';
   exit;
  }
 }
         
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus tahun ajaran <b class="text-red">'.$col->keterangan."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(10,$id))) {
   echo "1";
   exit;
  }
 }

}
