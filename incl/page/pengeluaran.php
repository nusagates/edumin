<?php

class page_pengeluaran {

 public $ctx;
 private $table = 'pengeluaran';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'pengeluaran');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT p.id, p.nama, p.nominal, p.tanggal,  kp.nama kategori, pg.nama admin FROM `pengeluaran` p 
LEFT JOIN kategori_pengeluaran kp
ON p.kategori=kp.id
LEFT JOIN pengguna pg
ON p.admin=pg.id limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT p.id, p.nama, p.nominal, p.tanggal,  kp.nama kategori, pg.nama admin FROM `pengeluaran` p 
LEFT JOIN kategori_pengeluaran kp
ON p.kategori=kp.id
LEFT JOIN pengguna pg
ON p.admin=pg.id limit $start, $length");
   $query->execute(array("%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->nama, "Rp. ".number_format($col->nominal, 0, "", ","),$col->tanggal, $col->kategori, $col->admin, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('nama', $nama))exit(text('required', 'Tujuan Pengeluaran'));
  if (General::s_post('kategori', $kategori))exit(text('required', 'Kategori Pengeluaran'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tujuan Pengeluaran'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  General::s_post('tanggal', $tanggal);
  General::s_post('keterangan', $keterangan);
  $query = $this->ctx->db->prepare("insert into $this->table(nama, kategori, tahun, nominal, tanggal, keterangan, admin) values(?,?,?,?,?,?,?)");
  if ($query->execute(array($nama, $kategori, $tahun, $nominal, $tanggal, $keterangan, $_SESSION['id-pengguna']))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Tujuan Pengeluaran", 12, $col->nama, '1', "text");
   General::html_select_db($this->ctx->db, "kategori_pengeluaran", "kategori", "Kategori Pengeluaran", 6, "where status=1", $col->kategori);
   General::html_select_db($this->ctx->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "",$col->tahun);
   General::html_input("nominal", "Nominal", 6, $col->nominal, '1', "number");
   General::html_input("tanggal", "Tanggal", 6, $col->tanggal, '1', "text");
   General::html_textarea("keterangan", "Keterangan", 12, $col->keterangan);
   General::html_info();
   echo '</form>';
  }
 }
 


 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Tujuan Pengeluaran'));
  if (General::s_post('kategori', $kategori))exit(text('required', 'Kategori Pengeluaran'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tujuan Pengeluaran'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  General::s_post('tanggal', $tanggal);
  General::s_post('keterangan', $keterangan);
  $db = $this->ctx->db;
  $query = $db->prepare("update pengeluaran set nama=?, kategori=?, tahun=?, nominal=?, tanggal=?, keterangan=?, updated=? where id=?");
  if ($query->execute(array($nama, $kategori, $tahun, $nominal, $tanggal, $keterangan, $_SESSION['id-pengguna'],$id))) {
   echo '1';
   exit;
  }
  General::error_PDO($db);
 }

         
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query1 = $this->ctx->db->prepare("select * from $this->table");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data siswa <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(10,$id))) {
   echo "1";
   exit;
  }
 }
 function kategori() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'kategori_pengeluaran');
  }
 }
  function kategori_read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT * from kategori_pengeluaran where status=1 order by nama asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("select * from kategori_pengeluaran where nama=? and status=? order by nama asc limit $start, $length");
   $query->execute(array("%".$search."%", 1));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->nama, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
function kategori_insert(){
  if (General::s_post('nama', $nama)) exit(text('required', 'Nama Kelas'));
  $query = $this->ctx->db->prepare("insert into kategori_pengeluaran(nama) values(?)");
  if($query->execute(array($nama))){
   echo "1";
  }else{
   echo "Gagal menambahkan kategori pemasukan";
  }
 }
 
 function kategori_edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_pengeluaran where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Nama Kelas", 12, $col->nama, '1');
   General::html_info();
   echo '</form>';
  }
 }
 
 function kategori_update(){
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Kategori'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_pengeluaran set nama=? where $this->id=?");
  if ($query->execute(array($nama,  $id))) {
   echo '1';
   exit;
  }
 }
 function kategori_delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_pengeluaran where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
    echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus kategori pengeluaran <b class="text-red">'.$col->nama."</b> dari master data?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }
 function kategori_delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_pengeluaran set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }

}
