<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
class page_laporan {

 public $ctx;
 private $table = 'biaya';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'laporan');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id
UNION
SELECT k.tanggal as tgl, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k limit $start, $length");
   $query->execute(array("%".$search."%", "%".$search."%", "%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $kas_masuk = $col->kas_masuk==""?"-":"Rp. ".number_format($col->kas_masuk, 2, ".", ",");
   $kas_keluar = $col->kas_keluar==0?"-":"Rp. ".number_format($col->kas_keluar, 2, ".", ",");
   $data[]=array($col->tanggal, $col->uraian,  $kas_masuk, $kas_keluar);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 function data_chart() {
  header("content-type: application/json");
  General::s_post('tahun', $id_siswa);
  $query = $this->ctx->db->prepare("select year(tanggal) tahun, month(tanggal) bulan,  sum(kas_masuk) kas_masuk, sum(kas_keluar) kas_keluar from(SELECT m.tanggal as tanggal,  m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id
UNION
SELECT k.tanggal as tgl,  '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k) f GROUP BY year(tanggal), month(tanggal)");
  $query->execute();
  $data = array();
  if($query->rowCount()){
   while($col=$query->fetchObject()){
    $dateObj   = DateTime::createFromFormat('!m', $col->bulan);
    $bulan = $dateObj->format('F'); 
    array_push($data, array(
        "bulan"=>array($bulan, $col->tahun),
        "masuk"=>$col->kas_masuk,
        "keluar"=>$col->kas_keluar
    ));
   }
   echo json_encode($data);
  }else{
   echo json_encode(array(
        "bulan"=>"",
        "masuk"=>0,
        "keluar"=>0
    ));
  }
 }
 
 function kas() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'arus_kas');
  }
 }
 function kas_read(){
  header("content-type: application/json");
  General::s_post('mulai', $mulai);
  General::s_post('akhir', $akhir);
  $db = $this->ctx->db;
   if(empty($mulai) && empty($akhir)){
    $query = $db->query("SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc");
   }else{
    $query = $db->prepare("SELECT DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, uraian, kas_masuk, kas_keluar FROM(
SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id 
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc
)d where tanggal>=? and tanggal <=?");
    $query->execute(array($mulai, $akhir));
   }
  
  while($col=$query->fetchObject()){
   $kas_masuk = $col->kas_masuk==""?"-":"Rp. ".number_format($col->kas_masuk, 2, ".", ",");
   $kas_keluar = $col->kas_keluar==0?"-":"Rp. ".number_format($col->kas_keluar, 2, ".", ",");
   $data[]=array($col->tanggal, $col->uraian,  $kas_masuk, $kas_keluar);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
 function kas_cetak_pdf() {
  
   $title ="Arus Kas";
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
  $pdf->SetHeaderData('/logo.jpg', 20, HEADER_TITLE_1 , HEADER_TITLE_2."\n".HEADER_TITLE_3 , array(0, 0, 0), array(0, 64, 128));
  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 14));
  $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  
  $pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Edumin Indonesia');
$pdf->SetTitle('Laporan Arus Kas');
$pdf->SetSubject('Administrasi Keuangan');
$pdf->SetKeywords('Laporan Keuangan, Administrasi Sekolah');
  
  $pdf->AddPage();
  General::s_post('mulai', $mulai);
  General::s_post('akhir', $akhir);
  $db = $this->ctx->db;
   if(empty($mulai) && empty($akhir)){
    $query = $db->query("SELECT DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, uraian, kas_masuk, kas_keluar FROM(
SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id 
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc
)d");
   }else{
    $query = $db->prepare("SELECT DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, uraian, kas_masuk, kas_keluar FROM(
SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id 
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc
)d where tanggal>=? and tanggal <=?");
    $query->execute(array($mulai, $akhir));
    $title .=" $mulai - $akhir";
   }

  $html = '
          <style>
          table, td, th{border: 1px solid #eee; padding: 5px}
          .number{text-align: right}
          .foot, .head{background-color: #eee}
          .document-title{text-align:center;margin-top:20px;padding-bottom:10px}
          .document-title div{font-size: 14pt; text-decoration: underline}
          .document-title span{font-size: 10pt;}
          </style>
          <div class="document-title">
          <div>LAPORAN ARUS KAS</div>
          <table width="1085" cellspacing="0" >
          <thead>
          <tr class="head">
          <th width="120px">TANGGAL</th>
         <th >URAIAN</th>
         <th width="120px">MASUK</th>
         <th width="120px">KELUAR</th>
          </tr> 
          </thead>
          <tbody>';
  $i = 0;
  $total_kas_masuk =0;
  $total_kas_keluar =0;
  if($query->rowCount()){
   while ($col = $query->fetchObject()) {
   setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
   $kas_masuk = $col->kas_masuk == "" ? "-" :  number_format($col->kas_masuk, 0, ".", ",");
   $kas_keluar = $col->kas_keluar == 0 ? "-" :  number_format($col->kas_keluar, 0, ".", ",");
   $tanggal = DateTime::createFromFormat ( "Y-m-d", $col->tanggal );
   $html .= '<tr><td width="120px" style="white-space: nowrap;">' . $tanggal->format('d M Y') . '</td>';
   $html .= '<td>' . $col->uraian . '</td>';
   $html .= '<td width="120px" class="number">' . $kas_masuk . '</td>';
   $html .= '<td width="120px" class="number">' . $kas_keluar . '</td></tr>';
   $total_kas_masuk +=is_numeric($col->kas_masuk)?$col->kas_masuk:0;
   $total_kas_keluar +=is_numeric($col->kas_keluar)?$col->kas_keluar:0;
   $i++;
  }
  }
  $html .='<tr class="foot"><td align="center" colspan="2"><b>TOTAL</b></td><td class="number"><b>Rp. '.number_format($total_kas_masuk, 0, ".", ",").'</b></td><td class="number"><b>Rp. '.number_format($total_kas_keluar, 0, ".", ",").'</b></td></tr>';
  $html .= '</tbody></table>
</body>
</html>';

  $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
  $pdf->Output($title.'.pdf', 'D');
  
 }
function kas_cetak_excel() {
   General::s_post('mulai', $mulai);
  General::s_post('akhir', $akhir);
  $title ="Arus Kas ";
  $db = $this->ctx->db;
   if(empty($mulai) && empty($akhir)){
    $query = $db->query("SELECT DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, uraian, kas_masuk, kas_keluar FROM(
SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id 
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc
)d");
   }else{
    $query = $db->prepare("SELECT DATE_FORMAT(tanggal, '%Y-%m-%d') tanggal, uraian, kas_masuk, kas_keluar FROM(
SELECT m.tanggal as tanggal, b.nama as uraian, m.dibayar as kas_masuk, '' as kas_keluar FROM pemasukan m
LEFT JOIN biaya b
ON m.biaya=b.id 
UNION
SELECT k.tanggal as tanggal, k.nama as uraian, '' as kas_masuk, k.nominal as kas_keluar FROM pengeluaran k order by tanggal desc
)d where tanggal>=? and tanggal <=?");
    $query->execute(array($mulai, $akhir));
    $title .=" $mulai - $akhir";
   }
   
   $spreadsheet = new Spreadsheet();
   $spreadsheet->getProperties()
    ->setCreator("Edumin Indonesia")
    ->setLastModifiedBy("Edumin Indonesia")
    ->setTitle("Laporan Arus Kas")
    ->setSubject("Laporan Arus Kas")
    ->setDescription(
        "Laporan Arus Kas Sekolah dibuat oleh Edumin Indonesia"
    )
    ->setKeywords("Laporan Keuangan")
    ->setCategory("Laporan Keuangan");
  
$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(35);
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$spreadsheet->getActiveSheet()->getStyle('A3:D3')->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('00B35A');
$spreadsheet->getActiveSheet()->getStyle('A3:D3')
    ->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'LAPORAN ARUS KAS');
$spreadsheet->getActiveSheet()->getStyle('A1')
    ->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$spreadsheet->getActiveSheet()->mergeCells("A1:D1");
$sheet->setCellValue('A3', 'TANGGAL');
$sheet->setCellValue('B3', 'URAIAN');
$sheet->setCellValue('C3', 'KAS MASUK');
$sheet->setCellValue('D3', 'KAS KELUAR');
$i=4;
if($query->rowCount()){
   while ($col = $query->fetchObject()) {
    $kas_masuk = $col->kas_masuk == "" ? "-" :  number_format($col->kas_masuk, 0, ".", ".");
    $kas_keluar = $col->kas_keluar == 0 ? "-" :  number_format($col->kas_keluar, 0, ".", ".");
    $sheet->setCellValue('A'.$i, $col->tanggal);
    $sheet->setCellValue('B'.$i, $col->uraian);
    $sheet->setCellValue('C'.$i, $col->kas_masuk);
    $sheet->setCellValue('D'.$i, $col->kas_keluar);
    $i++;
   }
   $rowTotal = $i+1;
   $sheet->setCellValue('C'.$i, "=SUM(C4:C$i)");
   $sheet->setCellValue('D'.$i, "=SUM(D4:D$i)");
   $sheet->setCellValue("A$i", "TOTAL");
   $spreadsheet->getActiveSheet()->mergeCells("A$i:B$i");
   $spreadsheet->getActiveSheet()->getStyle("A$i:D$i")->getFill()
    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
    ->getStartColor()->setARGB('00B35A');
   $spreadsheet->getActiveSheet()->getStyle("A3:D$i")->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
   $spreadsheet->getActiveSheet()->getStyle("C4:D$i")->getNumberFormat()
    ->setFormatCode('#,##0');
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
}

}
