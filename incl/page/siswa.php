<?php

class page_siswa {

 public $ctx;
 private $table = 'sumber_pemasukan';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'siswa');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT sp.id, sp.kode, sp.nama, k.nama kelas_sekarang FROM `sumber_pemasukan` sp 
LEFT JOIN kelas k 
ON sp.kelas_sekarang=k.id WHERE sp.ksp=1 and sp.status=1 order by sp.kode asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT sp.id, sp.kode, sp.nama, k.nama kelas_sekarang FROM `sumber_pemasukan` sp 
LEFT JOIN kelas k 
ON sp.kelas_sekarang=k.id WHERE sp.ksp=1 and sp.status=1 and sp.nama like ? order by sp.kode asc limit $start, $length");
   $query->execute(array("%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $nama = "<a target='_blank' href='".$this->ctx->base_url."/tagihan/$col->id'>$col->nama</a>";
   $data[]=array("<input class='pilih-siswa' value='$col->id' type='checkbox' name='checkboxvar[]'>",$col->kode, $nama,$col->kelas_sekarang, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('nisn', $nisn))exit(text('required', 'NISN'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Masuk'));
  if (General::s_post('kelas', $kelas))exit(text('required', 'Kelas Sekarang'));
  if (General::s_post('nama-wali', $nama_wali))exit(text('required', 'Nama Wali'));
  if (General::s_post('hp-wali', $hp_wali))exit(text('required', 'HP Wali'));
  $expr = '/^[1-9][0-9]*$/';
  //if(!preg_match($expr, $hp_wali))exit("Nomor HP harus berupa angka");
  if (General::s_post('alamat', $alamat))exit(text('required', 'Alamat'));
  $db = $this->ctx->db;
  $cek = $db->prepare("select nisn from $this->table where nisn=?");
  $cek->execute(array($nisn));
  if($cek->rowCount())exit("NISN Sudah terdaftar dalam sistem");
  $query = $db->prepare("insert into $this->table(ksp, kode, nama, tahun_masuk, kelas_sekarang, nama_wali, hp_wali, alamat) values(?, ?,?,?,?,?,?,?)");
  if ($query->execute(array(1, $nisn, $nama, $tahun, $kelas, $nama_wali, $hp_wali, $alamat))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nisn", "NISN", 6, $col->kode, '1', "number", "readonly");
   General::html_input("nama", "Nama Siswa", 6, $col->nama, '1', "text");
   General::html_select_db($this->ctx->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1", $col->tahun_masuk);
   General::html_select_db($this->ctx->db, "kelas", "kelas", "Kelas Sekarang", 6, "where status=1", $col->kelas_sekarang);
   General::html_input("nama-wali", "Nama Wali", 6, $col->nama_wali, '1', "text");
   General::html_input("hp-wali", "HP Wali", 6, $col->hp_wali, '1', "phone");
   General::html_textarea("alamat", "Alamat", "12", $col->alamat);
   General::html_info();
   echo '</form>';
  }
 }
 
 function naik_form(){
  if(!isset($_POST['id']))exit(Text('required', "ID"));
  $id_array = $_POST['id'];
  $in  = str_repeat('?,', count($id_array) - 1) . '?';
  $kelas_sama = TRUE;
  $query = $this->ctx->db->prepare("SELECT sp.kelas_sekarang id_kelas, sp.id, sp.kode, sp.nama, k.nama kelas_sekarang FROM `sumber_pemasukan` sp 
LEFT JOIN kelas k 
ON sp.kelas_sekarang=k.id where sp.id in ($in) order by kode asc");
  $query->execute($id_array);
  echo "<p class='col-md-12'>Daftar siswa yang akan naik kelas adalah sebagai berikut:</p>";
  echo "<ol>";
  $i=0;
  while ($col=$query->fetchObject()){
   if($i==0){
    $k = $col->kelas_sekarang;
    $id_kelas = $col->id_kelas;
   }
   $kelas_sekarang = $col->kelas_sekarang;
   if($k!=$kelas_sekarang){
    ?>
<script>
 swal("Peringatan!","Silahkan pilih siswa yang kelasnya sama.", "warning");
 $("#btn-update-kelas").hide();
</script>
<?php
$kelas_sama = FALSE;
   }
   
   $nama = $col->nama;
   echo "<li><em>$nama</em> kelas <b>$kelas_sekarang</b></li>";
   $i++;
  }
  echo "</ol>";
  if($kelas_sama==TRUE){
   echo '<form id="form-naik-kelas" method="post">';
   General::html_select_db($this->ctx->db, "kelas", "kelas", "Kelas Tujuan", 6, "where status=1", $id_kelas);
   echo "<input type='hidden' name='id' value='". base64_encode(serialize($id_array))."'/>";
   echo '</form>';
   ?>
<script>
 $("#btn-update-kelas").slideDown();
 </script>
<?php
  }
 }

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nisn', $nisn))exit(text('required', 'NISN'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Masuk'));
  if (General::s_post('kelas', $kelas))exit(text('required', 'Kelas Sekarang'));
  if (General::s_post('nama-wali', $nama_wali))exit(text('required', 'Nama Wali'));
  if (General::s_post('hp-wali', $hp_wali))exit(text('required', 'HP Wali'));
  if (General::s_post('alamat', $alamat))exit(text('required', 'Alamat'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set nama=?, tahun_masuk=?, kelas_sekarang=?, nama_wali=?, hp_wali=?, alamat=? where $this->id=?");
  if ($query->execute(array($nama, $tahun, $kelas, $nama_wali, $hp_wali, $alamat, $id))) {
   echo '1';
   exit;
  }
 }
 function naik_update(){
  if (General::s_post('kelas', $kelas))exit(text('required', 'Kelas Tujuan'));
  $array_id= unserialize(base64_decode($_POST['id']));
  $id_count = count($array_id);
  $in  = str_repeat('?,', $id_count - 1) . '?';
  $query = $this->ctx->db->prepare("select * from $this->table where id in ($in)");
  $query->execute($array_id);
  $i=0;
  while ($col=$query->fetchObject()){
   $kelas_sekarang = $col->kelas_sekarang;
   $update = $this->ctx->db->prepare("update $this->table set kelas_sekarang=? where id=? and kelas_sekarang=?");
   if($update->execute(array($kelas, $col->id, $kelas_sekarang))){
    if($i===$id_count-1){
     echo "1";
    }
   }else{
    echo "Gagal menaikkan siswa ke kelas lebih tinggi";
   }
   $i++;
  }
 }
         
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query1 = $this->ctx->db->prepare("select * from $this->table");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data siswa <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(10,$id))) {
   echo "1";
   exit;
  }
 }
 function kelas() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'kelas');
  }
 }
  function kelas_read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT * from kelas where status=1 order by nama asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("select * from kelas where nama=? and status=? order by nama asc limit $start, $length");
   $query->execute(array("%".$search."%", 1));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->nama, $col->kapasitas, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
function kelas_insert(){
  if (General::s_post('nama', $nama)) exit(text('required', 'Nama Kelas'));
  if (General::s_post('kapasitas', $kapsitas)) exit(text('required', 'Kapsitas'));
  $query = $this->ctx->db->prepare("insert into kelas(nama, kapasitas) values(?,?)");
  if($query->execute(array($nama, $kapsitas))){
   echo "1";
  }else{
   echo "Gagal menambahkan kategori pemasukan";
  }
 }
 
 function kelas_edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kelas where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Nama Kelas", 12, $col->nama, '1');
   General::html_input("kapasitas", "Kapasitas", 12, $col->kapasitas, '1');
   General::html_info();
   echo '</form>';
  }
 }
 
 function kelas_update(){
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Kelas'));
  if (General::s_post('kapasitas', $kapasitas))exit(text('required', 'Kapasitas'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kelas set nama=?, kapasitas=? where $this->id=?");
  if ($query->execute(array($nama, $kapasitas, $id))) {
   echo '1';
   exit;
  }
 }
 function kelas_delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kelas where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
    echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus kelas <b class="text-red">'.$col->nama."</b> dari master data?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }
 function kelas_delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kelas set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }

}
