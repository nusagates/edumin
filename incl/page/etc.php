<?php

class page_etc {

 public $ctx;

 function __construct($ctx) {

  $this->ctx = $ctx;
 }

 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'laporan');
  }
  
 }

 function unAuthorized() {
  $this->ctx->_load_template($this, 'unauthorized_template');
 }



}
