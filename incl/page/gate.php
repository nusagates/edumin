<?php

class page_gate {

 public $ctx;

 function __construct($ctx) {
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  $this->ctx->_load_template($this, 'gate');
 }
 
 function login(){
  if (General::s_post('email', $email))exit(text('required', 'Email'));
  if (General::s_post('password', $password))exit(text('required', 'Password'));
  $db = $this->ctx->db;
  $query= $db->prepare("select * from pengguna where email=?");
  $query->execute(array($email));
  if($query->rowCount()){
  $col = $query->fetchObject();
   if(password_verify($password, $col->password)){
   $_SESSION['logged-in'] = true;
   $_SESSION['id-pengguna']=$col->id;
   $_SESSION['nama-pengguna']=$col->nama;
   $_SESSION['grup-pengguna']=$col->grup;
   echo "1";
   exit;
  }else{
   echo "Password tidak cocok.";
  }
  }else{
   echo "Pengguna tidak ditemukan";
  }
 }
 
 function logout(){
  session_unset();
  session_destroy();
  header("location:".$this->ctx->base_url."/gate");
 }

}
