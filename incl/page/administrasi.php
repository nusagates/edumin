<?php

class page_administrasi {

 public $ctx;
 private $table = 'pemasukan';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'administrasi');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT p.id, s.nama siswa, b.nama biaya, p.dibayar, p.tanggal, u.nama admin FROM `pemasukan` p 
LEFT JOIN sumber_pemasukan s 
ON p.siswa=s.id
LEFT JOIN biaya b 
ON p.biaya=b.id
LEFT JOIN pengguna u 
ON p.admin=u.id where p.status=1 order by p.ditambahkan desc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT p.id, s.nama siswa, b.nama biaya, p.dibayar, p.tanggal, u.nama admin FROM `pemasukan` p 
LEFT JOIN sumber_pemasukan s 
ON p.siswa=s.id
LEFT JOIN biaya b 
ON p.biaya=b.id
LEFT JOIN pengguna u 
ON p.admin=u.id where p.status=1 and (s.nama like ? or b.nama like ? or p.tanggal like ?) limit $start, $length");
   $query->execute(array("%".$search."%", "%".$search."%", "%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->siswa, $col->biaya, "Rp. ".number_format($col->dibayar, 2, ".", ","), $col->tanggal, $col->admin, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
 function autocomplete(){
  if (General::s_post('term', $term))exit(text('required', 'Nama Siswa'));
  $query = $this->ctx->db->prepare("select id, kode, nama, kelas_sekarang from sumber_pemasukan where nama like ? and status=? order by nama asc");
  $query->execute(array("%".$term."%", 1));
  $data = array();
  if ($query->rowCount()) {
   while ($col = $query->fetchObject()) {
    array_push($data, array(
        "id"=>$col->id,
        "label" => $col->nama, 
        "value" => $col->nama, 
        "nisn"=>$col->kode, 
        "kelas"=>$col->kelas_sekarang));
   }
   echo json_encode($data);
  }else{
   echo json_encode(array(
       "label"=>"Nama tidak ditemukan",
       "value"=>0));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  //print_r($_POST);exit;
  if (General::s_post('id-siswa', $id_siswa))exit(text('required', 'NISN'));
  if($id_siswa=="0")exit("Silahkan pilih Siswa");
  if (General::s_post('biaya', $biaya))exit(text('required_select', 'Item Biaya'));
  if($biaya=="Tidak Ada Item Biaya")exit("Silahkan pilih Item Biaya");
  if (General::s_post('dibayar', $dibayar))exit(text('required', 'Jumlah Pembayaran'));
  if (General::s_post('tanggal', $tanggal))exit(text('required', 'Tanggal Pembayaran'));

  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(siswa, biaya, dibayar, tanggal, admin) values(?,?,?,?,?)");
  if ($query->execute(array($id_siswa, $biaya, $dibayar, $tanggal, $_SESSION['id-pengguna']))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("SELECT p.status, p.id, s.nama siswa, b.tahun_ajaran, b.bulan, b.nama biaya, p.dibayar, p.tanggal, u.nama admin FROM `pemasukan` p 
LEFT JOIN sumber_pemasukan s 
ON p.siswa=s.id
LEFT JOIN biaya b 
ON p.biaya=b.id
LEFT JOIN pengguna u 
ON p.admin=u.id where p.id=?");
  $query->execute(array($id));
  if ($query->rowCount()) {
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nominal", "Nominal Pembayaran", 12, $col->dibayar, '1', "number", "step='500'");
   General::html_info();
   echo '</form>';
  }
 }
 

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal Pembayaran'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set dibayar=? where $this->id=?");
  if ($query->execute(array($nominal, $id))) {
   echo '1';
   exit;
  }
 }

         
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("SELECT p.status, p.id, s.nama siswa, b.tahun_ajaran, b.bulan, b.nama biaya, p.dibayar, p.tanggal, u.nama admin FROM `pemasukan` p 
LEFT JOIN sumber_pemasukan s 
ON p.siswa=s.id
LEFT JOIN biaya b 
ON p.biaya=b.id
LEFT JOIN pengguna u 
ON p.admin=u.id where p.id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data pembayaran siswa <b class="text-red">'.$col->siswa."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }
 
function biaya(){
  if (General::s_post('id', $id)) exit(text('required', 'ID Siswa'));
  if (General::s_post('tahun', $tahun)) exit(text('required', 'Tahun Ajaran'));
  $query = $this->ctx->db->prepare("SELECT * FROM `biaya` WHERE find_in_set(?, siswa) and tahun_ajaran = ?");
  $query->execute(array($id, $tahun));
  if($query->rowCount()){
   while ($col=$query->fetchObject()){
    $nominal = $col->nominal;
    $dibayar = General::item_dibayar($this->ctx->db, $id, $col->id);
    $status = number_format($nominal-$dibayar, 0, "", ",");
    if($dibayar==""){
     $keterangan = "Belum Dibayar";
    }else if($dibayar<$nominal){
     $keterangan = "Kurang $status";
    }else {
     $keterangan = "Lunas";
    }
    echo "<option value='$col->id'>$col->nama ($keterangan)</option>";
   }
  }else{
   echo "<option>Tidak Ada Item Biaya</option>";
  }
}

}
