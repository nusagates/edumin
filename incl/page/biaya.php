<?php

class page_biaya {

 public $ctx;
 private $table = 'biaya';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'biaya');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT bi.id, bi.nama, bi.nominal, t.keterangan tahun, bu.nama bulan, bi.status FROM `biaya` bi
LEFT JOIN tahun_ajaran t
ON bi.tahun_ajaran=t.id
LEFT JOIN bulan bu
ON bi.bulan=bu.id where bi.id!=10 order by bi.nama asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT bi.id, bi.nama, bi.nominal, t.keterangan tahun, bu.nama bulan, bi.status FROM `biaya` bi
LEFT JOIN tahun_ajaran t
ON bi.tahun_ajaran=t.id
LEFT JOIN bulan bu
ON bi.bulan=bu.id WHERE bi.id!=10 and (bi.nama like ? or t.keterangan like ? or bu.nama like ?)  order by nama asc limit $start, $length");
   $query->execute(array("%".$search."%", "%".$search."%", "%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $status = $col->status=="1"?"Aktif":"Tidak Aktif";
   $data[]=array($col->nama, "Rp. ".number_format($col->nominal, 2, ".", ","), $col->tahun,$col->bulan, $status, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  
  if (General::s_post('kategori', $kategori))exit(text('required', 'Kategori Pemasukan'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Pembiayaan'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Ajaran'));
  if (General::s_post('bulan', $bulan))exit(text('required', 'Bulan Pembiayaan'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  $id_siswa = isset($_POST['id-siswa'])?$_POST['id-siswa']:"";
  if(empty($id_siswa))exit ("Siswa tertagih wajib diisi");
  $id_siswa = array_unique($id_siswa);
  $id_str = implode(",", $id_siswa);
  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(kategori, nama, nominal, tahun_ajaran, bulan, siswa) values(?,?,?,?,?,?)");
  if ($query->execute(array($kategori, $nama, $nominal, $tahun, $bulan, $id_str))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $db = $this->ctx->db;
  $query = $db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if ($query->rowCount()) {
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_select_db($this->ctx->db, "kategori_pemasukan", "kategori", "Kategori Pemasukan", 6, "where status=1", $col->id);
   General::html_input("nama", "Nama Pembiayaan", 6, $col->nama, '1', "text", "placeholder='contoh: SPP'");
   General::html_select_db($db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1", $col->tahun_ajaran);
   General::html_select_db($db, "bulan", "bulan", "Bulan", 6, "", $col->bulan);
   General::html_input("nominal", "Nominal", 6, $col->nominal, '1', "number");
   General::ambil_kelas($this->ctx->db, "Filter Tertagih", "kelas1",6);
              echo "<div class='col-md-12'>"
              . "<div class='form-group'>"
              . "<fieldset>"
              . "<legend>Hasil Filter</legend>"
              . "<div class='form-inline' id='hasil-filter1'></div>"
              . "<button class='btn btn-info' id='btn-tagih1'>Tambahkan ke Daftar Tertagih</button>"
              . "</fieldset>"
              . "</div>"
              . "</div>";
              echo "<div class='col-md-12'>"
              . "<div class='form-group'>"
              . "<fieldset>"
              . "<legend>Daftar Tertagih</legend>"
              . "<div class='form-inline' id='siswa-tertagih1'>";
              $array_id = explode(',', $col->siswa);
              $in  = str_repeat('?,', count($array_id) - 1) . '?';
              $siswa = $db->prepare("select id, kode, nama from sumber_pemasukan where id in($in)");
              $siswa->execute($array_id);
              if($siswa->rowCount()){
               while($kol=$siswa->fetchObject()){
                echo "<label><input name='id-siswa[]' type='checkbox' value='$kol->id' checked/> $kol->nama ($kol->kode) </label> ";
               }
              }
              echo "</div>"
              . "</fieldset>"
              . "</div>"
              . "</div>";
   General::html_checkbox("status", "Item Pembayaran Aktif?", 12, $col->status);
   General::html_info();
   echo '</form>';
  }?>
<script>
 var base_url = '<?php echo $this->ctx->base_url ?>/biaya';
$(function(){
 $("#kelas1").change(function () {
         var id = $(this).val();
         getSiswa1(id);
     });
     getSiswa1("1");
 function getSiswa1(id) {
         $.ajax({
             url: base_url + "/siswa",
             dataType: 'text',
             method: "post",
             data: {filter: id},
             success: function (response) {
                 $("#hasil-filter1").html(response);
                 $("#btn-tagih1").click(function (e) {
                     e.preventDefault();
                     var data = $("#hasil-filter1 input:checkbox:checked");
                     data.parent().appendTo("#siswa-tertagih1");
                     $("#siswa-tertagih1 input:checkbox").attr("name", "id-siswa[]");
                     $("#siswa-tertagih1 input:checkbox").change(function () {
                         if (!$(this).is(":checked")) {
                             $(this).removeAttr("name");
                             $(this).parent().appendTo("#hasil-filter1");
                         }
                     });
                 });
             },
             error: function () {}
         });
     }
     $("#siswa-tertagih1 input:checkbox").change(function () {
                         if (!$(this).is(":checked")) {
                             $(this).removeAttr("name");
                             $(this).parent().appendTo("#hasil-filter1");
                         }
                     });
})
</script>
<?php
 }
 

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('kategori', $kategori))exit(text('required', 'Kategori Pemasukan'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Pembiayaan'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Ajaran'));
  if (General::s_post('bulan', $bulan))exit(text('required', 'Bulan Tagihan'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  $id_siswa = isset($_POST['id-siswa'])?$_POST['id-siswa']:"";
  if(empty($id_siswa))exit ("Siswa tertagih wajib diisi");
  $id_siswa = array_unique($id_siswa);
  $id_str = implode(",", $id_siswa);
  General::s_post('status', $status);
  if(empty($status)){
   $status = "0";
  }else{
   $status = "1";
  }
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set kategori=?, nama=?, nominal=?, tahun_ajaran=?, bulan=?, siswa=?, status=? where $this->id=?");
  if ($query->execute(array($kategori, $nama, $nominal, $tahun, $bulan, $id_str, $status, $id))) {
   echo '1';
   exit;
  }
 }

         
 function delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query1 = $this->ctx->db->prepare("select * from $this->table");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data biaya <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(10,$id))) {
   echo "1";
   exit;
  }
 }
 function siswa(){
  General::s_post('filter', $id);
  if($id==0){
   $query = $this->ctx->db->prepare("select id, kode,  nama from sumber_pemasukan");
   $query->execute();
  }else{
   $query = $this->ctx->db->prepare("select id, kode,  nama from sumber_pemasukan where kelas_sekarang=?");
   $query->execute(array($id));
  }
  if($query->rowCount()){
   while($col = $query->fetchObject()){
   echo " <label><input type='checkbox' value='$col->id' checked/> $col->nama ($col->kode) </label> ";
   }
  }else{
   echo "Tidak ada siswa pada kelas dipilih";
  }
 }
 function kategori(){
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'kategori_pemasukan');
  }
 }
 function kategori_read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT * from kategori_pemasukan where status=1 order by nama asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("select * from kategori_pemasukan where nama=? and status=? order by nama asc limit $start, $length");
   $query->execute(array("%".$search."%", 1));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->nama, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
 function kategori_insert(){
  if (General::s_post('nama', $nama)) exit(text('required', 'Nama Kategori'));
  $query = $this->ctx->db->prepare("insert into kategori_pemasukan(nama) values(?)");
  if($query->execute(array($nama))){
   echo "1";
  }else{
   echo "Gagal menambahkan kategori pemasukan";
  }
 }
  function kategori_edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Nama Kategori", 12, $col->nama, '1');
   General::html_info();
   echo '</form>';
  }
 }
 function kategori_update(){
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Kategori'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_pemasukan set nama=? where $this->id=?");
  if ($query->execute(array($nama, $id))) {
   echo '1';
   exit;
  }
 }
 
  function kategori_delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus kategori pemasukan <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }
 
 function kategori_delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_pemasukan set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }
 function kategori_sumber(){
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'kategori_sumber_pemasukan');
  }
 }
 function kategori_sumber_read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT * from kategori_sumber_pemasukan where status=1 order by nama asc limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("select * from kategori_sumber_pemasukan where nama=? and status=? order by nama asc limit $start, $length");
   $query->execute(array("%".$search."%", 1));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->nama, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
 function kategori_sumber_insert(){
  if (General::s_post('nama', $nama)) exit(text('required', 'Nama Kategori'));
  $query = $this->ctx->db->prepare("insert into kategori_sumber_pemasukan(nama) values(?)");
  if($query->execute(array($nama))){
   echo "1";
  }else{
   echo "Gagal menambahkan kategori pemasukan";
  }
 }
  function kategori_sumber_edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_sumber_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Nama Kategori", 12, $col->nama, '1');
   General::html_info();
   echo '</form>';
  }
 }
 function kategori_sumber_update(){
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Kategori'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_sumber_pemasukan set nama=? where $this->id=?");
  if ($query->execute(array($nama, $id))) {
   echo '1';
   exit;
  }
 }
 
  function kategori_sumber_delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_sumber_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   if($col->id==1){
    echo '<div class="col-md-12">Data ini tidak bisa dihapus.</div>';
   }else{
    echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus kategori sumber pemasukan <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
   }
  }
 }
 
 function kategori_sumber_delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_sumber_pemasukan set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }
 
 function sumber(){
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'sumber_pemasukan');
  }
 }
 function sumber_read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT sp.id, sp.kode, sp.nama, sp.hp_wali, ks.nama kategori FROM `sumber_pemasukan` sp
LEFT JOIN kategori_sumber_pemasukan ks
ON sp.ksp=ks.id  WHERE sp.status =1 ORDER BY sp.kode ASC limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT sp.id, sp.kode, sp.nama, sp.hp_wali, ks.nama kategori FROM `sumber_pemasukan` sp
LEFT JOIN kategori_sumber_pemasukan ks
ON sp.ksp=ks.id  WHERE sp.status =1 ORDER BY sp.kode ASC limit $start, $length");
   $query->execute(array("%".$search."%", 1));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $data[]=array($col->kode, $col->nama, $col->hp_wali, $col->kategori, $menu);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }
 
 function sumber_insert(){
  if (General::s_post('kategori', $kategori))exit(text('required', 'Kategori'));
  if (General::s_post('kode', $kode))exit(text('required', 'KODE/NISN'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama'));
  if($kategori=="1"){
   if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Masuk'));
   if (General::s_post('kelas', $kelas))exit(text('required', 'Kelas Sekarang'));
   if (General::s_post('nama-wali', $nama_wali))exit(text('required', 'Nama Wali'));
  }else{
  error_reporting(E_ALL);
ini_set('display_errors', 1);
   General::s_post('tahun', $tahun);
   General::s_post('kelas', $kelas);
   General::s_post('nama-wali', $nama_wali);
  }
  if (General::s_post('hp-wali', $hp_wali))exit(text('required', 'HP Wali'));
  $expr = '/^[1-9][0-9]*$/';
  //if(!preg_match($expr, $hp_wali))exit("Nomor HP harus berupa angka");
  General::s_post('alamat', $alamat);
  $db = $this->ctx->db;
  $cek = $db->prepare("select kode from sumber_pemasukan where kode=?");
  $cek->execute(array($kode));
  if($cek->rowCount())exit("KODE/NISN Sudah terdaftar dalam sistem");
  $query = $this->ctx->db->prepare("insert into kategori_pemasukan(nama) values(?)");
  $query = $db->prepare("insert into $this->table(ksp, kode, nama, tahun_masuk, kelas_sekarang, nama_wali, hp_wali, alamat) values(?,?,?,?,?,?,?,?)");
  if ($query->execute(array($kategori, $kode, $nama, $tahun, $kelas, $nama_wali, $hp_wali, $alamat))) {
   echo '1';
   exit;
  }else{
  print_r($_POST);
  echo "Gagal";
  }
 }
  function sumber_edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from sumber_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_select_db($this->ctx->db, "kategori_sumber_pemasukan", "kategori", "Kategori", 6, "where status=1", $col->ksp);
   General::html_input("kode", "KODE/NISN", 6, $col->kode, '1', "number", "readonly");
   General::html_input("nama", "Nama Siswa", 6, $col->nama, '1', "text");
   General::html_select_db($this->ctx->db, "kelas", "kelas", "Kelas", 6, "where status=1", $col->kelas_sekarang);
   echo "<div id='additional-container'>";
   General::html_select_db($this->ctx->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 6, "where status=1", $col->tahun_masuk);
   General::html_input("nama-wali", "Nama Wali", 6, $col->nama_wali, '1', "text");
   echo "</div>";
   General::html_input("hp-wali", "Kontak", 6, $col->hp_wali, '1', "phone");
   General::html_textarea("alamat", "Alamat", "12", $col->alamat);
   General::html_info();
   echo '</form>';
  }
  ?>
<script>
   $(function(){
    var kategori_id = $("#modal-edit #kategori").val();
    if (kategori_id !== "1") {
               $("#modal-edit #additional-container").slideUp();
           } else {
             $("#modal-edit #additional-container").slideDown();
           }
    
    $("#modal-edit #kategori").change(function () {
           var kategori_id = $(this).val();
           if (kategori_id !== "1") {
               $("#modal-edit #additional-container").slideUp();
           } else {
             $("#modal-edit #additional-container").slideDown();
           }
       });
   });
</script>
<?php
 }
 function sumber_update(){
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('kategori', $kategori))exit(text('required', 'KODE/NISN'));
  if (General::s_post('kode', $nisn))exit(text('required', 'KODE/NISN'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Masuk'));
  if (General::s_post('kelas', $kelas))exit(text('required', 'Kelas Sekarang'));
  if (General::s_post('nama-wali', $nama_wali))exit(text('required', 'Nama Wali'));
  if (General::s_post('hp-wali', $hp_wali))exit(text('required', 'Kontak'));
  if (General::s_post('alamat', $alamat))exit(text('required', 'Alamat'));
  $db = $this->ctx->db;
  $query = $db->prepare("update sumber_pemasukan set ksp=?, nama=?, tahun_masuk=?, kelas_sekarang=?, nama_wali=?, hp_wali=?, alamat=? where id=?");
  if ($query->execute(array($kategori, $nama, $tahun, $kelas, $nama_wali, $hp_wali, $alamat, $id))) {
   echo '1';
   exit;
  }
 }
 
  function sumber_delete_form() {
 
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from kategori_pemasukan where id=?");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus kategori pemasukan <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }
 
 function sumber_delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update kategori_pemasukan set status=? where $this->id=?");
  if ($query->execute(array(0,$id))) {
   echo "1";
   exit;
  }
 }

}
