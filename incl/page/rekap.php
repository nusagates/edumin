<?php

class page_rekap {

 public $ctx;
 private $table = 'biaya';
 private $id = 'id';

 function __construct($ctx) {
  if (!$ctx->isUserLoggedIn()) {
   $ctx->_route('gate');
   exit;
  }
  $this->ctx = $ctx;
 }

 /**
  * fungsi untuk menampilkan halaman index kategori
  */
 function index() {
  if (!$this->ctx->isUserLoggedIn()) {
   $this->ctx->_route('gate');
   exit;
  }else{
   $this->ctx->_load_template($this, 'rekap');
  }
 }
 
 function read(){
  $length = $_REQUEST['length'];
  $start = $_REQUEST['start'];
  $search = $_REQUEST['search']["value"];
  if(empty($search)){
   $query = $this->ctx->db->query("SELECT pe.id, si.kode, si.nama, bi.nama biaya, bi.nominal nominal, SUM(pe.dibayar) dibayar, nominal-dibayar kurang  FROM `pemasukan` pe 
LEFT JOIN sumber_pemasukan si 
ON pe.siswa=si.id
LEFT JOIN biaya bi 
ON pe.biaya=bi.id
GROUP BY pe.biaya, pe.siswa order by si.kode limit $start, $length");
  }else{
   $query = $this->ctx->db->prepare("SELECT pe.id, si.kode, si.nama, bi.nama biaya, bi.nominal nominal, SUM(pe.dibayar) dibayar, nominal-dibayar kurang  FROM `pemasukan` pe 
LEFT JOIN sumber_pemasukan si 
ON pe.siswa=si.id
LEFT JOIN biaya bi 
ON pe.biaya=bi.id
WHERE si.kode like ? or si.nama like ? or bi.nama like ? GROUP BY pe.biaya, pe.siswa ORDER BY si.nisn limit $start, $length");
   $query->execute(array("%".$search."%", "%".$search."%", "%".$search."%"));
  }
  while($col=$query->fetchObject()){
   $menu = "<a "
                    . "data-edit='" . $col->id. "' "
                    . "status='' "
                    . "class='btn-edit btn btn-xs btn-success' href='#'><i class='fa fa-edit'></i></a> "
                    . "<a data-hapus='" . $col->id . "' class='btn-hapus btn btn-xs btn-danger' href='#'><i class='fa fa-times'></i></a>";
   $nominal = $col->nominal;
   $dibayar = $col->dibayar;
   $krg = $nominal-$dibayar;
   if($krg>0){
    $kurang = "<label class='label label-danger'>"."Kurang Rp. ".number_format($krg, 2, ".", ",")."</label>";
   }else if($krg==0){
    $kurang = "<label class='label label-success'>Lunas</label>";
   }else{
    $kurang = "<label class='label label-warning'>"."Lebih Rp. ".number_format(str_replace("-", "", $krg), 2, ".", ",")."</label>";
   }
   $data[]=array($col->kode, $col->nama, $col->biaya, "Rp. ".number_format($nominal, 2, ".", ","), "Rp. ".number_format($dibayar, 2, ".", ","),$kurang);
  }
  if($query->rowCount()){
   echo json_encode(array(
   "recordsTotal" => count($data),
   "recordsFiltered" => count($data),
   "data" => $data
  ));
  }else{
   echo json_encode(array(
   "recordsTotal" => 0,
   "recordsFiltered" => 0,
   "data" => 0
  ));
  }
 }

 /**
  * Fungsi ini digunakan untuk menambah kategori baru
  * 
  */
 function insert() {
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Pembiayaan'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Ajaran'));
  if (General::s_post('bulan', $bulan))exit(text('required', 'Bulan Pembiayaan'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  General::s_post('status', $status);
  if(empty($status)){
   $status = "0";
  }else{
   $status = "1";
  }
  $db = $this->ctx->db;
  $query = $db->prepare("insert into $this->table(nama, nominal, tahun_ajaran, bulan, status) values(?,?,?,?,?)");
  if ($query->execute(array($nama, $nominal, $tahun, $bulan, $status))) {
   echo '1';
   exit;
  }
 }
 
 function edit_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query->execute(array($id));
  if ($query->rowCount()) {
   $col = $query->fetchObject();
   echo '<form id="form-edit" method="post">';
   General::html_input_hidden('id', $col->id);
   General::html_input("nama", "Nama Pembiayaan", 12, $col->nama, '1', "text", "placeholder='contoh: SPP'");
   General::html_select_db($this->ctx->db, "tahun_ajaran", "tahun", "Tahun Ajaran", 12, "where status=1", $col->tahun_ajaran);
   General::html_select_db($this->ctx->db, "bulan", "bulan", "Bulan", 12, "", $col->bulan);
   General::html_input("nominal", "Nominal", 12, $col->nominal, '1', "number");
   General::html_checkbox("status", "Aktif", 12, $col->status);
   General::html_info();
   echo '</form>';
  }
 }
 

 function update() {
  if (General::s_post('id', $id))exit(text('required', 'ID'));
  if (General::s_post('nama', $nama))exit(text('required', 'Nama Pembiayaan'));
  if (General::s_post('tahun', $tahun))exit(text('required', 'Tahun Ajaran'));
  if (General::s_post('bulan', $bulan))exit(text('required', 'Bulan Tagihan'));
  if (General::s_post('nominal', $nominal))exit(text('required', 'Nominal'));
  General::s_post('status', $status);
  if(empty($status)){
   $status = "0";
  }else{
   $status = "1";
  }
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set nama=?, nominal=?, tahun_ajaran=?, bulan=?, status=? where $this->id=?");
  if ($query->execute(array($nama, $nominal, $tahun, $bulan, $status, $id))) {
   echo '1';
   exit;
  }
 }

         
 function delete_form() {
  if(General::s_post("id", $id))exit(Text('required', "ID"));
  $query = $this->ctx->db->prepare("select * from $this->table where id=?");
  $query1 = $this->ctx->db->prepare("select * from $this->table");
  $query->execute(array($id));
  if($query->rowCount()){
   $col = $query->fetchObject();
   echo '<div class="col-md-12">Apakah Anda yakin ingin menghapus data siswa <b class="text-red">'.$col->nama."</b>?</div>";
   echo '<form id="form-hapus" method="post">';
   General::html_input_hidden('id', $id);
   echo '</form>';
  }
 }

 function delete() {
  if (General::s_post('id', $id)) exit(text('required', 'ID'));
  $db = $this->ctx->db;
  $query = $db->prepare("update $this->table set status=? where $this->id=?");
  if ($query->execute(array(10,$id))) {
   echo "1";
   exit;
  }
 }

}
