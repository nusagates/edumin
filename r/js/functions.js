/* 
 * Name: functions.js
 * Author: Ahmad Budairi
 * Author_URL: www.nusagates.com
 */

var tabel;
function fetch_data(tableID, url) {
    tabel = $(tableID).DataTable({
        "processing": true,
        responsive: true,
        "paging": true,
        "serverSide": true,
        columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        "ajax": {
            "url": url,
            "type": "post"
        }
    });

}
function add_data(url, data, infoBoxId, modalId, tabelId) {
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            if (s === '1') {
                //toastr.success('Data berhasil ditambahakan');
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
                $("#form-tambah").trigger('reset');
                swal("Selamat!","Data berhasil ditambahkan.", "success");
            }else{
                swal("Peringatan!",s, "warning");
            }
        }
    });
}

function edit_form(url, modalId, data, container) {
    $("" + modalId + "").modal("show");
    $.ajax({
        url: url,
        method: 'post',
        data: {id: data},
        success: function (s) {
            $("" + container + "").html(s);
        }
    });
}

function update_data(url, data, infoBoxId, modalId, tabelId) {
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            if (s === '1') {
                //toastr.success('Data berhasil ditambahakan');
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
                $("#form-tambah").trigger('reset');
                swal("Selamat!","Data berhasil diperbarui.", "success");
            }else{
                swal("Peringatan!",s, "warning");
            }
        }
    });
}

function delete_form(url, modalId, data, container) {
    $("" + modalId + "").modal("show");
    $.ajax({
        url: url,
        method: 'post',
        data: {id: data},
        success: function (s) {
            $("" + container + "").html(s);
        }
    });
}

function remove(url, data, modalId, tabelId) {
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        success: function (s) {
            if (s === '1') {
                $('' + modalId + '').modal("hide");
                $('' + tabelId + '').DataTable().ajax.reload();
                swal("Selamat!","Data berhasil dihapus.", "success");
            }
        }
    });
}

toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

$.datetimepicker.setLocale('id');

